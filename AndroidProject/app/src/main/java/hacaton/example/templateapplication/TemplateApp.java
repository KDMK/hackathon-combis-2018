package hacaton.example.templateapplication;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Application root!
 */
public class TemplateApp extends Application {

    private static TemplateApp templateApp;

    public static TemplateApp getTemplateApp() {
        return templateApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        FlowManager.init(new FlowConfig.Builder(this).build());
        templateApp = this;
    }

}