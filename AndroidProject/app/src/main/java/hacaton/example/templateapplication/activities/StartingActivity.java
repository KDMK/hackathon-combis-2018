package hacaton.example.templateapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hacaton.example.templateapplication.R;
import hacaton.example.templateapplication.activities.newElement.AddNewElementActivity;
import hacaton.example.templateapplication.network.executor.NetworkExecutor;

public class StartingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.starting_activity_toolbar)
    Toolbar startingActivityToolbar;

    @BindView(R.id.activity_main_navigation_view)
    NavigationView navigationView;

    @BindView(R.id.activity_main_drawer_layout)
    DrawerLayout drawerLayout;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);
        ButterKnife.bind(this);

        initializeDrawerBehaviour();

        setSupportActionBar(startingActivityToolbar);
        getSupportActionBar().setTitle("Template application");

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    private void initializeDrawerBehaviour() {
        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                startingActivityToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_open) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);

//        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NetworkExecutor.getInstance().destroyAnyPendingTransactions();
    }

    @OnClick(R.id.cameraActivityButton)
    public void showCameraActivity() {
        Intent intent = new Intent(StartingActivity.this, AddNewElementActivity.class);
        startActivity(intent);
        finish();
    }
}