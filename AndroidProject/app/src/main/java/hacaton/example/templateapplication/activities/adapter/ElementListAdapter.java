package hacaton.example.templateapplication.activities.adapter;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hacaton.example.templateapplication.R;
import hacaton.example.templateapplication.model.CustomModel;
import hacaton.example.templateapplication.util.Util;

public class ElementListAdapter extends RecyclerView.Adapter<ElementListAdapter.ViewHolder> {

    private final List<CustomModel> customModels = new ArrayList<>();
    private final Context context;

    public ElementListAdapter(Context context, List<CustomModel> customModels) {
        this.context = context;
        this.customModels.addAll(customModels);

        notifyItemRangeInserted(0, customModels.size());
    }

    public int size() {
        return customModels.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CustomModel customModel = customModels.get(position);
        holder.elementName.setText(customModel.getName());

        try {
            if (Util.fileExists(Uri.parse(customModel.getImageSource()), context) || customModel.getImageSource().startsWith("http")) {
                Picasso.with(context).load(customModels.get(position).getImageSource()).into(holder.elementPicture);
            } else {
                Picasso.with(context).load(R.drawable.ic_person_details).into(holder.elementPicture);
            }
        } catch (NullPointerException ex) {
            Picasso.with(context).load(R.drawable.ic_person_details).into(holder.elementPicture);
        }
    }

    @Override
    public int getItemCount() {
        return customModels.size();
    }

    public void addElement(CustomModel newCustomModel) {
        if (!customModels.contains(newCustomModel)) {
            customModels.add(0, newCustomModel);
            notifyItemInserted(0);
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_name)
        TextView elementName;

        @BindView(R.id.adapter_image_view)
        ImageView elementPicture;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
