package hacaton.example.templateapplication.activities.base;


public interface BaseActivityMVP {

    interface Presenter extends BaseMVP.Presenter {

    }


    interface View extends BaseMVP.View {

        void showProgress(String title, String message);

        void hideProgress();
    }

}