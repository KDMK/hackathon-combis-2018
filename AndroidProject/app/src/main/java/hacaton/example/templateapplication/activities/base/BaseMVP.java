package hacaton.example.templateapplication.activities.base;


public interface BaseMVP {

    interface View {

        void showError(String errorMessage);

    }

    interface State {
    }

    interface Presenter {

        void cancelCall();

    }

    interface Interactor {

        void cancel();

    }

}
