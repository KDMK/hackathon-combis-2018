package hacaton.example.templateapplication.activities.newElement;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import hacaton.example.templateapplication.R;
import hacaton.example.templateapplication.activities.base.BaseActivity;
import hacaton.example.templateapplication.util.Util;

public class AddNewElementActivity extends BaseActivity implements AddNewElementMVP.View {

    @BindView(R.id.add_element_image)
    ImageView imageView;

    @BindView(R.id.add_element_name)
    EditText name;

    @BindView(R.id.add_element_height)
    EditText height;

    @BindView(R.id.add_element_weight)
    EditText weight;

    @BindView(R.id.add_element_description)
    EditText description;

    @BindView(R.id.add_element_floating)
    FloatingActionButton addElementFloating;
    private AddNewElementMVP.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_element_activity);
        ButterKnife.bind(this);
        presenter = new AddNewElementPresenter(this);
    }

    @OnTextChanged(R.id.add_element_name)
    void onNameChanged() {
        presenter.onNameChanged(name.getText().toString());
    }


    @OnTextChanged(R.id.add_element_description)
    void onDescriptionChanged() {
        presenter.onDescriptionChanged(description.getText().toString());
    }

    @Override
    public void setName(String name) {
        this.name.setText(name);
    }

    @Override
    public void setDescription(String description) {
        this.description.setText(description);
    }

    @Override
    public void setWeight(String weight) {
        this.weight.setText(weight);
    }

    @Override
    public void setHeight(String height) {
        this.height.setText(height);
    }

    private void showRequestPermissionRationale(int message, String permissionGroupToBeAskedFor) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setMessage("Please give us permission!")
                .setPositiveButton(R.string.close, (dialogInterface, i) -> {

                    presenter.requestPermission(permissionGroupToBeAskedFor);
                    dialogInterface.cancel();
                });

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    @Override
    public void showDialogChooser() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View dialogView = LayoutInflater.from(this).inflate(R.layout.add_photo_dialog, null);
        builder.setView(dialogView);

        AlertDialog alertDialog = builder.create();

        Button takePhoto = (Button) dialogView.findViewById(R.id.button_take_photo);
        Button choosePhoto = (Button) dialogView.findViewById(R.id.button_choose_photo);

        takePhoto.setOnClickListener((v) -> {
            if (!Util.storageMounted()) {
                alertDialog.dismiss();
                showError("Storage not mounted");
                return;
            }

            presenter.onTakePhoto();
            alertDialog.dismiss();
        });

        choosePhoto.setOnClickListener((v) -> {
            if (!Util.storageMounted()) {
                alertDialog.dismiss();
                showError("Storage not mounted");
                return;
            }

            presenter.onGetImageFromGallery();
            alertDialog.dismiss();
        });

        alertDialog.show();
    }

    @Override
    public void setImage(Uri imageUri) {
        Picasso.with(this).load(imageUri).into(imageView);
    }

    @Override
    public void setImage(int drawable) {
        Picasso.with(this).load(drawable).into(imageView);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        presenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }


    @OnClick(R.id.add_element_floating)
    void addImage() {
        presenter.addImage();
    }

}
