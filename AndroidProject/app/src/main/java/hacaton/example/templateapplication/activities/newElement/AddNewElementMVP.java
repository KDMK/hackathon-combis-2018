package hacaton.example.templateapplication.activities.newElement;

import android.content.Intent;
import android.net.Uri;

import hacaton.example.templateapplication.activities.base.BaseActivityMVP;
import hacaton.example.templateapplication.activities.base.BaseMVP;
import hacaton.example.templateapplication.model.CustomModel;
import hacaton.example.templateapplication.network.callback.CallBackHandler;

/**
 *
 */

public interface AddNewElementMVP {

    interface View extends BaseActivityMVP.View {

        void setName(String name);

        void setDescription(String description);

        void setWeight(String weight);

        void setHeight(String height);

        void showDialogChooser();

        void setImage(Uri imageUri);

        void setImage(int drawable);
    }

    interface Presenter extends BaseActivityMVP.Presenter {

        void onNameChanged(String name);

        void onDescriptionChanged(String description);

        void onButtonSaveCLicked();

        void addImage();

        void requestPermission(String permission);

        void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void onTakePhoto();

        void onGetImageFromGallery();

        boolean checkDataValidity();
    }

    interface Interactor extends BaseMVP.Interactor {

        void saveElementToServer(CustomModel customModel, CallBackHandler callback);

        void saveElementToDatabase(CustomModel customModel);

    }
}