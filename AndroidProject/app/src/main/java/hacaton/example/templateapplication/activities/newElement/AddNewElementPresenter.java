package hacaton.example.templateapplication.activities.newElement;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.IOException;

import hacaton.example.templateapplication.model.CustomModel;
import hacaton.example.templateapplication.network.callback.CallBackHandler;
import hacaton.example.templateapplication.util.Util;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 *
 */
public class AddNewElementPresenter implements AddNewElementMVP.Presenter {
    private static final int PICTURE_FROM_STORAGE_REQUEST_CODE = 120;
    private static final int PICTURE_WITH_CAMERA_REQUEST_CODE = 180;
    private static final int PERMISSION_STORAGE_REQUEST_CODE = 22;

    private String name;
    private String description;
    private String height;
    private String weight;
    private String imageUri;

    private AddNewElementMVP.View view;
    private AddNewElementMVP.Interactor interactor;

    public AddNewElementPresenter(AddNewElementMVP.View view) {
        this.view = view;
        interactor = null;
    }

    @Override
    public void onNameChanged(String name) {
        this.name = name;
    }

    @Override
    public void onDescriptionChanged(String description) {
        this.description = description;
    }

    @Override
    public void onButtonSaveCLicked() {
        if (!Util.internetConnectionActive()) {
            view.showError("No active internet connection.");
            return;
        }

        if (!dataValid()) {
            view.showError("Invalid data");
            return;
        }

        CustomModel customModel = new CustomModel();
        customModel.setName(name);
        customModel.setDescription(description);
        customModel.setNumberOfChildren(Double.parseDouble(weight));
        customModel.setAge(Double.parseDouble(height));
        customModel.setImageSource(imageUri);

        view.showProgress("Saving customModel", "In progress..");
        interactor.saveElementToServer(customModel, new CallBackHandler() {
            @Override
            public void onSuccess(Object object) {
                CustomModel createdCustomModel = (CustomModel) object;

                interactor.saveElementToDatabase(createdCustomModel);

                view.hideProgress();
            }

            @Override
            public void onFailure(String message) {
                view.hideProgress();
                view.showError(message);
            }

            @Override
            public void onCancel() {
                view.hideProgress();
            }
        });
    }

    private boolean dataValid() {
        try {
            Double.parseDouble(height);
            Double.parseDouble(weight);
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    @Override
    public void addImage() {
        checkForPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void requestPermission(String permission) {
//        ((AddNewElementActivity) view).requestPermissions(new String[]{permission},
//                PERMISSION_STORAGE_REQUEST_CODE
//        );
    }

    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    view.showDialogChooser();
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            if (requestCode == PICTURE_FROM_STORAGE_REQUEST_CODE && data != null) {
                imageUri = data.getData().toString();
            }

            view.setImage(Uri.parse(imageUri));
        } else if (resultCode == RESULT_CANCELED) {
            imageUri = null;
        }
    }

    @Override
    public void onTakePhoto() {
        Activity context = (AddNewElementActivity) view;

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = Util.createImageFile();
            } catch (IOException ex) {
                view.showError("Can not make photo file.");
            }

            final String authorities = context.getApplicationContext().getPackageName() + ".fileprovider";
            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(context,
                        authorities,
                        photoFile);

                imageUri = photoUri.toString();

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                context.startActivityForResult(takePictureIntent, PICTURE_WITH_CAMERA_REQUEST_CODE);
            } else {
                view.showError("Cannot take photo!");
            }

        }
    }

    @Override
    public void onGetImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");

        Activity context = (AddNewElementActivity) view;

        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivityForResult(intent, PICTURE_FROM_STORAGE_REQUEST_CODE);
        } else {
            view.showError("Cannot open gallery!");
        }
    }

    @Override
    public boolean checkDataValidity() {
        return dataValid();
    }

    private void checkForPermission(String permission) {
        Activity context = (AddNewElementActivity) view;

        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context, permission)) {
                showExplanationAndRequestPermission(permission);
            } else {
                requestPermission(permission);
            }
        } else {
            view.showDialogChooser();
        }
    }

    private void showExplanationAndRequestPermission(String permissionGroupToBeExplainedToTheUser) {
//        switch (permissionGroupToBeExplainedToTheUser) {
//            case android.Manifest.permission.WRITE_EXTERNAL_STORAGE:
//                view.showRequestPermissionRationale("Please give us permission", permissionGroupToBeExplainedToTheUser);
//                break;
//        }
    }

    @Override
    public void cancelCall() {

    }
}