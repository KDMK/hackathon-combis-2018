package hacaton.example.templateapplication.database;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import hacaton.example.templateapplication.model.CustomModel;


public class DatabaseImpl implements SQLLite {
    @Override
    public List<CustomModel> getAll() {
        return SQLite.select().from(CustomModel.class).flowQueryList();
    }

    @Override
    public void save(CustomModel[] elements) {
        for (CustomModel element : elements) {
            element.save();
        }
    }

    @Override
    public void add(CustomModel CustomModel) {
        CustomModel.save();
    }

    @Override
    public void update(CustomModel CustomModel) {
        CustomModel.update();
    }

    @Override
    public void remove(CustomModel CustomModel) {
        CustomModel.delete();
    }

    @Override
    public void clearTable() {
        List<CustomModel> elements = SQLite.select().from(CustomModel.class).flowQueryList();

        for (CustomModel element : elements) {
            element.delete();
        }
    }
}