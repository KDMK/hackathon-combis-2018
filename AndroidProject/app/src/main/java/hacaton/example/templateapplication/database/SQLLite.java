package hacaton.example.templateapplication.database;


import java.util.List;

import hacaton.example.templateapplication.model.CustomModel;

public interface SQLLite {

    List<CustomModel> getAll();

    void save(CustomModel[] CustomModel);

    void add(CustomModel CustomModel);

    void update(CustomModel CustomModel);

    void remove(CustomModel CustomModel);

    void clearTable();
}