package hacaton.example.templateapplication.model;

import android.support.annotation.NonNull;

import hacaton.example.templateapplication.database.AppDatabase;
import hacaton.example.templateapplication.network.executor.ServiceCreator;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.squareup.moshi.Json;

import java.io.Serializable;

@Table(database = AppDatabase.class)
public class CustomModel extends BaseModel implements Serializable, Comparable<CustomModel> {

    @PrimaryKey
    private String id;

    @Column
    @Json(name = "image-url")
    private String imageSource;

    @Column
    @Json(name = "name")
    private String name;

    @Column
    @Json(name = "description")
    private String description;

    @Column
    @Json(name = "age")
    private double age;

    @Column
    @Json(name = "numberOfChildren")
    private double numberOfChildren;

    public CustomModel() {
    }

    public CustomModel(String name, String description, double age, double weight, String imageSource) {
        this.name = name;
        this.description = description;
        this.age = age;
        this.numberOfChildren = weight;
        this.imageSource = imageSource;
    }

    public String getImageSource() {
        if (imageSource != null && imageSource.startsWith("/")) {
            return ServiceCreator.API_ENDPOINT + imageSource;
        }

        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public double getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(double numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    @Override
    public int compareTo(@NonNull CustomModel o) {
        return Integer.parseInt(o.getId()) - Integer.parseInt(getId());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}