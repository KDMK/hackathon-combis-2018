package hacaton.example.templateapplication.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.squareup.moshi.Json;

import hacaton.example.templateapplication.database.AppDatabase;

@Table(database = AppDatabase.class)
public class ResponseData {

    @PrimaryKey
    @Json(name = "id")
    private String id;

    @Column
    @Json(name = "data")
    private String data;

    @Column
    @Json(name = "message")
    private String message;

    @Column
    @Json(name = "success")
    private boolean success;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}