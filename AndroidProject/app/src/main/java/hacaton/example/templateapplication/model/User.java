package hacaton.example.templateapplication.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.squareup.moshi.Json;

import hacaton.example.templateapplication.database.AppDatabase;

@Table(database = AppDatabase.class)
public class User {

    @PrimaryKey
    @Json(name = "id")
    private String id;

    @Column
    @Json(name = "username")
    private String userName;

    @Column
    @Json(name = "email")
    private String email;

    @Column
    @Json(name = "password")
    private String password;

    @Column
    @Json(name = "password_confirmation")
    private String confirmationPassword;

    @Column
    @Json(name = "auth-token")
    private String authenticationToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    public void setConfirmationPassword(String confirmationPassword) {
        this.confirmationPassword = confirmationPassword;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}