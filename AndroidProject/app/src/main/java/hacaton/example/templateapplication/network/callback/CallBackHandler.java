package hacaton.example.templateapplication.network.callback;

public interface CallBackHandler {
    void onSuccess(Object object);

    void onFailure(String message);

    void onCancel();
}
