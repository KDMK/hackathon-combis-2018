package hacaton.example.templateapplication.network.executor;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import hacaton.example.templateapplication.model.CustomModel;
import hacaton.example.templateapplication.model.User;
import hacaton.example.templateapplication.network.callback.CallBackHandler;
import hacaton.example.templateapplication.network.executor.errorHandler.ErrorHandler;
import hacaton.example.templateapplication.network.services.CustomModelService;
import hacaton.example.templateapplication.network.services.UserService;
import hacaton.example.templateapplication.util.UserUtil;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkExecutor {

    private static NetworkExecutor networkExecutor;
    private volatile List<Call<?>> calls = new ArrayList<>();

    public static NetworkExecutor getInstance() {
        if (networkExecutor == null) {
            networkExecutor = new NetworkExecutor();
        }

        return networkExecutor;
    }

    public void destroyAnyPendingTransactions() {
        for (Call<?> call : calls) {
            call.cancel();
        }
    }

    private String getAuthenticationString() {
        final String authToken = UserUtil.getLoggedUser().getAuthenticationToken();
        final String email = UserUtil.getLoggedUser().getEmail();
        return "Token token=" + authToken + ", email=" + email;
    }

    public void signUp(User user, CallBackHandler callBack) {
        UserService userService = ServiceCreator.getUserService();
        Call<String> motherfucker = userService.getMotherfucker();

        calls.add(motherfucker);

        motherfucker.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                System.out.println("Success!");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("Failure!");
            }
        });

    }

    public void logIn(User user, CallBackHandler callBack) {
        UserService userService = ServiceCreator.getUserService();

        Call<User> call = userService.loginUser(user);

        calls.add(call);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                calls.remove(call);
                if (response.isSuccessful()) {
                    User responseUser = response.body();

                    callBack.onSuccess(responseUser);
                } else {
                    callBack.onFailure(ErrorHandler.getErrorResponse(response).toString());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                calls.remove(call);
                if (call.isCanceled()) {
                    callBack.onCancel();
                } else {
                    callBack.onFailure("Failure");
                }
            }
        });

    }

    public void logOut(CallBackHandler callBack) {
        UserService userService = ServiceCreator.getUserService();

        Call<Void> call = userService.logoutUser(getAuthenticationString());

        calls.add(call);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                calls.remove(call);
                if (response.isSuccessful()) {
                    callBack.onSuccess(null);
                } else {
                    callBack.onFailure(ErrorHandler.getErrorResponse(response).toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                calls.remove(call);
                if (call.isCanceled()) {
                    callBack.onCancel();
                } else {
                    callBack.onFailure("Failure");
                }
            }
        });


    }

    public void getAllElements(CallBackHandler callBack) {
        CustomModelService modelService = ServiceCreator.getModelService();


        Call<CustomModel[]> call = modelService.getAll(getAuthenticationString());

        calls.add(call);

        call.enqueue(new Callback<CustomModel[]>() {
            @Override
            public void onResponse(@NonNull Call<CustomModel[]> call, @NonNull Response<CustomModel[]> response) {
                calls.remove(call);
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onFailure(ErrorHandler.getErrorResponse(response).toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomModel[]> call, @NonNull Throwable t) {
                calls.remove(call);
                if (call.isCanceled()) {
                    callBack.onCancel();
                } else {
                    callBack.onFailure("Failure");
                }
            }
        });

    }

    public void createElement(CustomModel customModel, CallBackHandler callBack, Context context) {
        CustomModelService modelService = ServiceCreator.getModelService();

        RequestBody requestBody = createRequestBodyWithImageToBeSent(customModel, context);
        Call<CustomModel> call = modelService
                .createModel(getAuthenticationString(), customModel.getName(),
                        customModel.getAge(), customModel.getNumberOfChildren(), false, requestBody);

        calls.add(call);

        call.enqueue(new Callback<CustomModel>() {
            @Override
            public void onResponse(@NonNull Call<CustomModel> call, @NonNull Response<CustomModel> response) {
                calls.remove(call);
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onFailure(ErrorHandler.getErrorResponse(response).toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomModel> call, @NonNull Throwable t) {
                calls.remove(call);
                if (call.isCanceled()) {
                    callBack.onCancel();
                } else {
                    callBack.onFailure("Failure");
                }
            }
        });


    }

    public void getElement(int id, CallBackHandler callBack) {
        CustomModelService pokeService = ServiceCreator.getModelService();


        Call<CustomModel> call = pokeService.getById(getAuthenticationString(), id);
        calls.add(call);

        call.enqueue(new Callback<CustomModel>() {
            @Override
            public void onResponse(@NonNull Call<CustomModel> call, @NonNull Response<CustomModel> response) {
                calls.remove(call);
                if (response.isSuccessful()) {
                    callBack.onSuccess(response.body());
                } else {
                    callBack.onFailure(ErrorHandler.getErrorResponse(response).toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomModel> call, @NonNull Throwable t) {
                calls.remove(call);
                if (call.isCanceled()) {
                    callBack.onCancel();
                } else {
                    callBack.onFailure("Failure");
                }
            }
        });
    }

    private RequestBody createRequestBodyWithImageToBeSent(CustomModel customModel, Context context) {
        String imageUriString = customModel.getImageSource();
        if (imageUriString == null) {
            return null;
        }

        Uri imageUri = Uri.parse(imageUriString);

        String filePathString = getPath(imageUri, context);
        File file = null;
        if (filePathString == null) {
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            String rootDirPath = storageDir.getAbsolutePath();
            String lastPart = rootDirPath.substring(rootDirPath.lastIndexOf('/') + 1);
            int indexOfConcatPath = imageUriString.indexOf(lastPart) + lastPart.length() + 1;
            String additionalFilePath = imageUriString.substring(indexOfConcatPath);
            file = new File(storageDir, additionalFilePath);
        }

        if (file == null) {
            file = new File(filePathString);
        } else if (!file.exists()) {
            return null;
        }

        return RequestBody.create(MediaType.parse("image/*"), file);
    }

    private String getPath(Uri imageUri, Context context) {
        String[] dataType = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(imageUri, dataType, null, null, null);
        int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        if (columnIndex == -1) {
            return null;
        }
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }
}