package hacaton.example.templateapplication.network.executor;

import android.util.Log;

import com.squareup.moshi.Moshi;

import hacaton.example.templateapplication.network.adapters.DateAdapter;
import hacaton.example.templateapplication.network.adapters.UriAdapter;
import hacaton.example.templateapplication.network.services.CustomModelService;
import hacaton.example.templateapplication.network.services.UserService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ServiceCreator {

    public static final String API_ENDPOINT = "http://hedonistic-zagreb.herokuapp.com";

    private static Retrofit restCreator;

    static {
        createRestAdapter();
    }

    public static UserService getUserService() {
        return restCreator.create(UserService.class);
    }

    public static CustomModelService getModelService() {
        return restCreator.create(CustomModelService.class);
    }

    private static void createRestAdapter() {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor(
                        (message) -> {
                            Log.d("logging_interceptor", message);
                        }
                ).setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client
                = new OkHttpClient.Builder()
                .addNetworkInterceptor(httpLoggingInterceptor)
                .build();

        Moshi moshi = new Moshi.Builder()
                .add(new DateAdapter())
                .add(new UriAdapter())
                .build();


        restCreator = new Retrofit.Builder().baseUrl(API_ENDPOINT)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .client(client).build();
    }
}