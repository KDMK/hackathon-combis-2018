package hacaton.example.templateapplication.network.executor.errorHandler;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

import retrofit2.Response;

public class ErrorHandler {

    public static ErrorHandler getErrorResponse(Response response) {

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ErrorHandler> jsonAdapter = moshi.adapter(ErrorHandler.class);

        ErrorHandler errorHandler;
        try {
            String errorBody = response.errorBody().string();
            errorHandler = jsonAdapter.fromJson(errorBody);
        } catch (IOException e) {
            errorHandler = new ErrorHandler();
        }

        return errorHandler;
    }
}