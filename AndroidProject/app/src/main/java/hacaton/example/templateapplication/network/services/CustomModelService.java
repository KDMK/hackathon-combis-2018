package hacaton.example.templateapplication.network.services;


import hacaton.example.templateapplication.model.CustomModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 *
 */

public interface CustomModelService {

    @GET("/api/element/all")
    Call<CustomModel[]> getAll(@Header("Authorization") String authHeader);

    @GET("/api/element/{id}")
    Call<CustomModel> getById(@Header("Authorization") String authHeader, @Path("id") int id);

    @Multipart
    @POST("/api/element")
    Call<CustomModel> createModel(@Header("Authorization") String authHeader,
                                  @Part(value = "data[attributes][name]", encoding = "text/plain") String name,
                                  @Part("data[attributes][height]") double age,
                                  @Part("data[attributes][weight]") double numberOfChildren,
                                  @Part("data[attributes][is_default]") boolean isDefault,
                                  @Part("data[attributes][image]\"; filename=\"image.jpg") RequestBody image);
}