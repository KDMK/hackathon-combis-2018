package hacaton.example.templateapplication.network.services;


import hacaton.example.templateapplication.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface UserService {

    @POST("/api/users/login")
    Call<User> loginUser(@Body User user);

    @DELETE("/api/users/logout")
    Call<Void> logoutUser(@Header("Authorization") String authHeader);

    @GET("/api/venue/get?category_id=6")
    Call<String> getMotherfucker();
}