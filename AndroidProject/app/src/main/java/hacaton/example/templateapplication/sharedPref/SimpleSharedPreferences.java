package hacaton.example.templateapplication.sharedPref;

import android.content.Context;

import hacaton.example.templateapplication.TemplateApp;
import hacaton.example.templateapplication.model.User;

public class SimpleSharedPreferences {

    private static final String NAME = "shared.prefs.userName";
    private static final String EMAIL = "shared.prefs.email";
    private static final String TOKEN = "shared.prefs.token";

    private static SimpleSharedPreferences instance;

    private final Context context;

    private SimpleSharedPreferences(Context context) {
        this.context = context;
    }

    public static SimpleSharedPreferences getInstance() {
        if (instance == null) {
            instance = new SimpleSharedPreferences(TemplateApp.getTemplateApp());
        }

        return instance;
    }

    public String getEmail() {
        android.content.SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return preferences.getString(EMAIL, null);
    }

    public String getToken() {
        android.content.SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return preferences.getString(TOKEN, null);
    }

    public boolean isLoggedIn() {
        return getToken() != null && getEmail() != null;
    }

    public void logOutUser() {
        android.content.SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.remove(TOKEN).remove(EMAIL).apply();
    }

    public void logInUser(String email, String authToken) {
        android.content.SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN, authToken);
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public User getLoggedInUser() {
        android.content.SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);

        final String email = preferences.getString(EMAIL, null);
        final String token = preferences.getString(TOKEN, null);

        User loggedInUser = new User();
        loggedInUser.setEmail(email);
        loggedInUser.setAuthenticationToken(token);

        return loggedInUser;
    }
}