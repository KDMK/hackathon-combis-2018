package hacaton.example.templateapplication.util;

import android.support.annotation.NonNull;

import java.util.regex.Pattern;

import hacaton.example.templateapplication.model.User;
import hacaton.example.templateapplication.sharedPref.SimpleSharedPreferences;

public class UserUtil {

    private static final String EMAIL_REGEX = ".+\\@[^\\+]+\\.[^\\+]+";

    public static boolean isLoggedIn() {
        return SimpleSharedPreferences.getInstance().isLoggedIn();
    }

    public static void logOut() {
        SimpleSharedPreferences.getInstance().logOutUser();
    }

    public static boolean validEmail(String email) {
        return Pattern.compile(EMAIL_REGEX).matcher(email).matches();
    }

    public static void logIn(@NonNull User user) {
        final String userAuthToken = user.getAuthenticationToken();
        final String userEmail = user.getEmail();
        final String username = user.getUserName();

        if (userAuthToken == null || userEmail == null || username == null) {
            throw new IllegalArgumentException("Illegal user values!");
        }

        SimpleSharedPreferences.getInstance().logInUser(userEmail, userAuthToken);
    }

    public static User getLoggedUser() {
        return SimpleSharedPreferences.getInstance().getLoggedInUser();
    }

}