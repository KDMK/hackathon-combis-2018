package hacaton.example.templateapplication.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import hacaton.example.templateapplication.TemplateApp;

public class Util {

    public static boolean internetConnectionActive() {
        ConnectivityManager connectivityManager = (ConnectivityManager) TemplateApp.getTemplateApp().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "element.jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File applicationDirectory = new File(storageDir, "/templateApp");
        if (!applicationDirectory.exists()) {
            applicationDirectory.mkdir();
        }

        return new File(applicationDirectory, imageFileName);
    }

    public static boolean fileExists(Uri file, Context context) {
        try (InputStream is = context.getContentResolver().openInputStream(file)) {
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static boolean storageMounted() {
        String storageState = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(storageState);
    }
}
