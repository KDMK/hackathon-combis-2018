package hackathon.dekoratori.projekt.controller.additionalData;

import hackathon.dekoratori.projekt.domain.additionalData.AdditionalData;
import hackathon.dekoratori.projekt.domain.bloodSupply.BloodSupply;
import hackathon.dekoratori.projekt.service.additionalData.AdditionalDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/additional-data")
public class AdditionalDataController {

    private final AdditionalDataService additionalDataService;

    @Autowired
    public AdditionalDataController(AdditionalDataService additionalDataService) {
        this.additionalDataService = additionalDataService;
    }

    @GetMapping("/{userId}")
    public List<AdditionalData> getAllForUser(@PathVariable final Long userId) {
        return this.additionalDataService.getAllForUser(userId);
    }

    @PostMapping
    public List<AdditionalData> edit(@RequestBody final List<AdditionalData> additionalData) {
        return this.additionalDataService.edit(additionalData);
    }
}
