package hackathon.dekoratori.projekt.controller.algorithm;

import hackathon.dekoratori.projekt.service.algorithm.AlgorithmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/algorithm")
public class AlgorithmController {

    @Autowired
    private final AlgorithmService algorithmService;

    @Autowired
    public AlgorithmController(AlgorithmService algorithmService) {
        this.algorithmService = algorithmService;
    }

    @GetMapping
    public AlgorithmDTO getAll() {
        return this.algorithmService.getData();
    }
}
