package hackathon.dekoratori.projekt.controller.algorithm;

import hackathon.dekoratori.projekt.domain.csv.CsvBloodData;

import java.util.List;
import java.util.Map;

//@Data
public class AlgorithmDTO {

    private List<CsvBloodData> csvBloodData;

    private Map<String, Long> bloodSupply;

    public AlgorithmDTO(List<CsvBloodData> csvBloodData, Map<String, Long> bloodSupply) {
        this.csvBloodData = csvBloodData;
        this.bloodSupply = bloodSupply;
    }

    public List<CsvBloodData> getCsvBloodData() {
        return csvBloodData;
    }

    public void setCsvBloodData(List<CsvBloodData> csvBloodData) {
        this.csvBloodData = csvBloodData;
    }

    public Map<String, Long> getBloodSupply() {
        return bloodSupply;
    }

    public void setBloodSupply(Map<String, Long> bloodSupply) {
        this.bloodSupply = bloodSupply;
    }
}