package hackathon.dekoratori.projekt.controller.bloodSupply;

import hackathon.dekoratori.projekt.domain.bloodSupply.BloodSupply;
import hackathon.dekoratori.projekt.service.bloodSupply.BloodSupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/blood-supplies")
public class BloodSupplyController {

    private final BloodSupplyService bloodSupplyService;

    @Autowired
    public BloodSupplyController(BloodSupplyService bloodSupplyService) {
        this.bloodSupplyService = bloodSupplyService;
    }

    @GetMapping
    public List<BloodSupply> getAll() {
        return this.bloodSupplyService.getAll();
    }

    @GetMapping("/{id}")
    public BloodSupply get(@PathVariable final Long id) {
        return this.bloodSupplyService.get(id);
    }

    @PostMapping
    public BloodSupply add(@RequestBody final BloodSupply bloodSupply) {
        return this.bloodSupplyService.add(bloodSupply);
    }

    @PutMapping
    public BloodSupply edit(@RequestBody final BloodSupply bloodSupply) {
        return this.bloodSupplyService.edit(bloodSupply);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        this.bloodSupplyService.delete(id);
    }
}
