package hackathon.dekoratori.projekt.controller.notification;

import hackathon.dekoratori.projekt.domain.notification.DonationRequestNotification;
import hackathon.dekoratori.projekt.service.notification.DonationRequestNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/donation-request-notification")
public class DonationRequestNotificationController {

    private final DonationRequestNotificationService donationRequestNotificationService;

    @Autowired
    public DonationRequestNotificationController(DonationRequestNotificationService donationRequestNotificationService) {
        this.donationRequestNotificationService = donationRequestNotificationService;
    }

    @GetMapping
    public List<DonationRequestNotification> getAll() {
        return this.donationRequestNotificationService.getAll();
    }

    @GetMapping("/{id}")
    public DonationRequestNotification get(@PathVariable final Long id) {
        return this.donationRequestNotificationService.get(id);
    }

    @PostMapping
    public DonationRequestNotification add(@RequestBody final DonationRequestNotification donationRequestNotification) {
        return this.donationRequestNotificationService.add(donationRequestNotification);
    }

    @PutMapping
    public DonationRequestNotification edit(@RequestBody final DonationRequestNotification donationRequestNotification) {
        return this.donationRequestNotificationService.edit(donationRequestNotification);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        this.donationRequestNotificationService.delete(id);
    }
}
