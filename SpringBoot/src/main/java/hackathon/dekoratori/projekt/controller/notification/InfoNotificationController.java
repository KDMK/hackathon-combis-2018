package hackathon.dekoratori.projekt.controller.notification;

import hackathon.dekoratori.projekt.domain.notification.InfoNotification;
import hackathon.dekoratori.projekt.service.notification.InfoNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/info-notification")
public class InfoNotificationController {

    private final InfoNotificationService infoNotificationService;

    @Autowired
    public InfoNotificationController(InfoNotificationService infoNotificationService) {
        this.infoNotificationService = infoNotificationService;
    }

    @GetMapping
    public List<InfoNotification> getAll() {
        return this.infoNotificationService.getAll();
    }

    @GetMapping("/{id}")
    public InfoNotification get(@PathVariable final Long id) {
        return this.infoNotificationService.get(id);
    }

    @PostMapping
    public InfoNotification add(@RequestBody final InfoNotification infoNotification) {
        return this.infoNotificationService.add(infoNotification);
    }

    @PutMapping
    public InfoNotification edit(@RequestBody final InfoNotification infoNotification) {
        return this.infoNotificationService.edit(infoNotification);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        this.infoNotificationService.delete(id);
    }
}
