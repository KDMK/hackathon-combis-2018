package hackathon.dekoratori.projekt.controller.pollData;

import hackathon.dekoratori.projekt.domain.pollData.PollData;
import hackathon.dekoratori.projekt.service.pollData.PollDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/poll-data")
public class PollDataController {

    private final PollDataService pollDataService;

    @Autowired
    public PollDataController(PollDataService pollDataService) {
        this.pollDataService = pollDataService;
    }

    @GetMapping
    public List<PollData> getAll() {
        return this.pollDataService.getAll();
    }

    @GetMapping("/{id}")
    public PollData get(@PathVariable final Long id) {
        return this.pollDataService.get(id);
    }

    @PostMapping
    public PollData add(@RequestBody final PollData pollData) {
        return this.pollDataService.add(pollData);
    }

    @PutMapping
    public PollData edit(@RequestBody final PollData pollData) {
        return this.pollDataService.edit(pollData);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        this.pollDataService.delete(id);
    }
}
