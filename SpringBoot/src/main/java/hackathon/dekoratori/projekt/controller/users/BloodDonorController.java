package hackathon.dekoratori.projekt.controller.users;

import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import hackathon.dekoratori.projekt.enumeration.BloodType;
import hackathon.dekoratori.projekt.enumeration.Gender;
import hackathon.dekoratori.projekt.service.users.BloodDonorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/blood-donors")
public class BloodDonorController {

    private final BloodDonorService bloodDonorService;

    @Autowired
    public BloodDonorController(BloodDonorService bloodDonorService) {
        this.bloodDonorService = bloodDonorService;
    }

    @GetMapping
    public List<BloodDonor> getAll() {
        return this.bloodDonorService.getAll();
    }

    @GetMapping("/{id}")
    public BloodDonor get(@PathVariable final Long id) {
        return this.bloodDonorService.get(id);
    }

    @PostMapping("/registration")
    public String addFromRegistration(@RequestBody final BloodDonorDto bloodDonorDto) {
        BloodDonor bloodDonor = new BloodDonor();
        bloodDonor.setBloodType(BloodType.valueOf(bloodDonorDto.getBloodType()));
        bloodDonor.setFirstName(bloodDonorDto.getFirstName());
        bloodDonor.setLastName(bloodDonorDto.getLastName());
        bloodDonor.setGender(Gender.getByName(bloodDonorDto.getGender()));
        bloodDonor.setUserId(bloodDonorDto.getUserId());
        BloodDonor add = this.bloodDonorService.add(bloodDonor);
        return "success";
    }

    @PostMapping
    public BloodDonor add(@RequestBody final BloodDonor bloodDonor) {
        return this.bloodDonorService.add(bloodDonor);
    }

    @PutMapping
    public BloodDonor edit(@RequestBody final BloodDonor bloodDonor) {
        return this.bloodDonorService.edit(bloodDonor);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        this.bloodDonorService.delete(id);
    }

    @GetMapping("/{doses}/{bloodType}")
    public void getByNumberOfDosesAndBloodType(@PathVariable final Long doses, @PathVariable final BloodType bloodType) {
        this.bloodDonorService.sendDonationRequests(doses, bloodType);
    }
}
