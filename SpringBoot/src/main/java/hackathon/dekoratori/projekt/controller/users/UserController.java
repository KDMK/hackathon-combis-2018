package hackathon.dekoratori.projekt.controller.users;

import hackathon.dekoratori.projekt.domain.users.Users;
import hackathon.dekoratori.projekt.service.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<Users> getAll() {
        return this.userService.getAll();
    }

    @GetMapping("/{id}")
    public Users get(@PathVariable final Long id) {
        return this.userService.get(id);
    }

    @PostMapping
    public Users add(@RequestBody final Users user) {
        return this.userService.add(user);
    }

    @PutMapping
    public Users edit(@RequestBody final Users user) {
        return this.userService.edit(user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable final Long id) {
        this.userService.delete(id);
    }

    @GetMapping("/logged")
    public List<GrantedAuthority> getLoggedUser() {
        List<GrantedAuthority> authorities = (List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return authorities;
    }
}
