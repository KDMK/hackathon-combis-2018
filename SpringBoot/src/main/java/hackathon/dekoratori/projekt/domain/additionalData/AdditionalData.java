package hackathon.dekoratori.projekt.domain.additionalData;

import hackathon.dekoratori.projekt.domain.pollData.PollData;
import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class AdditionalData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "poll_data_id", nullable = false)
    private PollData pollData;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "blood_donor_id", nullable = false)
    private BloodDonor bloodDonor;

    private Boolean value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PollData getPollData() {
        return pollData;
    }

    public void setPollData(PollData pollData) {
        this.pollData = pollData;
    }

    public BloodDonor getBloodDonor() {
        return bloodDonor;
    }

    public void setBloodDonor(BloodDonor bloodDonor) {
        this.bloodDonor = bloodDonor;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }
}
