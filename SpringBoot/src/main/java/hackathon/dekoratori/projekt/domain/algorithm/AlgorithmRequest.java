package hackathon.dekoratori.projekt.domain.algorithm;

import hackathon.dekoratori.projekt.domain.users.BloodDonor;

import java.util.List;

public class AlgorithmRequest {

    private List<BloodDonor> bloodDonors;

    private Long doses;

    public AlgorithmRequest(List<BloodDonor> bloodDonors, Long doses) {
        this.bloodDonors = bloodDonors;
        this.doses = doses;
    }

    public List<BloodDonor> getBloodDonors() {
        return bloodDonors;
    }

    public void setBloodDonors(List<BloodDonor> bloodDonors) {
        this.bloodDonors = bloodDonors;
    }

    public Long getDoses() {
        return doses;
    }

    public void setDoses(Long doses) {
        this.doses = doses;
    }
}
