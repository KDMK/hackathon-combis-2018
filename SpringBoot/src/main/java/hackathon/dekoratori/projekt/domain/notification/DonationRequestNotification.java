package hackathon.dekoratori.projekt.domain.notification;

import hackathon.dekoratori.projekt.enumeration.DonationRequestStatus;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class DonationRequestNotification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    private String title;

    private String content;

    @Enumerated(EnumType.STRING)
    private DonationRequestStatus donationRequestStatus;

    public DonationRequestNotification(Long userId, String title, String content, DonationRequestStatus donationRequestStatus) {
        this.userId = userId;
        this.title = title;
        this.content = content;
        this.donationRequestStatus = donationRequestStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public DonationRequestStatus getDonationRequestStatus() {
        return donationRequestStatus;
    }

    public void setDonationRequestStatus(DonationRequestStatus donationRequestStatus) {
        this.donationRequestStatus = donationRequestStatus;
    }
}
