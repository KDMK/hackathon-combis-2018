package hackathon.dekoratori.projekt.domain.poll;

import lombok.Data;
import org.apache.tomcat.jni.Poll;

import javax.persistence.*;

@Data
@Entity
public class PollItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String type;

    private String defaultValue;

}