package hackathon.dekoratori.projekt.domain.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import hackathon.dekoratori.projekt.domain.additionalData.AdditionalData;
import hackathon.dekoratori.projekt.domain.deserializers.LocalDateDeserializer;
import hackathon.dekoratori.projekt.enumeration.BloodType;
import hackathon.dekoratori.projekt.enumeration.Gender;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
public class BloodDonor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userId;

	private String firstName;

	private String lastName;

	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Enumerated(EnumType.STRING)
	private BloodType bloodType;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate lastDonation;

	private Double frequency;

	@JsonIgnore
	@OneToMany(mappedBy = "bloodDonor", fetch = FetchType.LAZY)
	private Set<AdditionalData> additionalData;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public BloodType getBloodType() {
		return bloodType;
	}

	public void setBloodType(BloodType bloodType) {
		this.bloodType = bloodType;
	}

	public LocalDate getLastDonation() {
		return lastDonation;
	}

	public void setLastDonation(LocalDate lastDonation) {
		this.lastDonation = lastDonation;
	}

	public Double getFrequency() {
		return frequency;
	}

	public void setFrequency(Double frequency) {
		this.frequency = frequency;
	}

	public Set<AdditionalData> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(Set<AdditionalData> additionalData) {
		this.additionalData = additionalData;
	}
}
