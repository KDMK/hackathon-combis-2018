package hackathon.dekoratori.projekt.enumeration;

import java.util.Arrays;

public enum BloodType {

    OM(),
    AM(BloodType.OM),
    BM(BloodType.OM),
    ABM(BloodType.OM, BloodType.AM, BloodType.BM),
    OP(BloodType.OM),
    AP(BloodType.OM, BloodType.AM, BloodType.OP),
    BP(BloodType.OM, BloodType.BM, BloodType.OP),
    ABP(BloodType.OM, BloodType.AM, BloodType.BM, BloodType.ABM, BloodType.OP, BloodType.AP, BloodType.BP);

    private BloodType[] donors;

    BloodType(BloodType... donors) {
        this.donors = Arrays.copyOf(donors, donors.length + 1);
        this.donors[donors.length] = this;
    }

    public BloodType[] getDonors() {
        return donors;
    }
}
