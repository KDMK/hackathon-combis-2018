package hackathon.dekoratori.projekt.enumeration;

public enum DonationRequestStatus {

    NOT_RESPONDED,
    ACCEPTED,
    DECLINED;
}
