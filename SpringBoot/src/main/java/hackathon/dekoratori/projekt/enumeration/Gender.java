package hackathon.dekoratori.projekt.enumeration;

public enum Gender {

    M(90L),
    Z(120L);

    private Long donationPeriod;

    Gender(Long donationPeriod) {
        this.donationPeriod = donationPeriod;
    }

    public Long getDonationPeriod() {
        return donationPeriod;
    }

    public static Gender getByName(final String name) {
        switch (name) {
            case "Z":
                return Gender.Z;
            default:
                return Gender.M;
        }
    }
}
