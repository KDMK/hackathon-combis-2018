package hackathon.dekoratori.projekt.repository.additionalData;

import hackathon.dekoratori.projekt.domain.additionalData.AdditionalData;
import hackathon.dekoratori.projekt.domain.bloodSupply.BloodSupply;
import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdditionalDataRepository extends JpaRepository<AdditionalData, Long> {

    List<AdditionalData> findAllByBloodDonor(BloodDonor bloodDonor);
}
