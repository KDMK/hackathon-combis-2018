package hackathon.dekoratori.projekt.repository.bloodSupply;

import hackathon.dekoratori.projekt.domain.bloodSupply.BloodSupply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BloodSupplyRepository extends JpaRepository<BloodSupply, Long> {
}
