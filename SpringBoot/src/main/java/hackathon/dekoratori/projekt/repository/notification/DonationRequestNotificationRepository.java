package hackathon.dekoratori.projekt.repository.notification;

import hackathon.dekoratori.projekt.domain.notification.DonationRequestNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DonationRequestNotificationRepository extends JpaRepository<DonationRequestNotification, Long> {
}
