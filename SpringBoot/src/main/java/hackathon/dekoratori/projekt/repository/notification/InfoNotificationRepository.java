package hackathon.dekoratori.projekt.repository.notification;

import hackathon.dekoratori.projekt.domain.notification.InfoNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfoNotificationRepository extends JpaRepository<InfoNotification, Long> {
}
