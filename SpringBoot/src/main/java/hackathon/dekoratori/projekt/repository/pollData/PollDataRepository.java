package hackathon.dekoratori.projekt.repository.pollData;

import hackathon.dekoratori.projekt.domain.pollData.PollData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollDataRepository extends JpaRepository<PollData, Long> {
}
