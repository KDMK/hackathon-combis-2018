package hackathon.dekoratori.projekt.repository.users;

import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import hackathon.dekoratori.projekt.enumeration.BloodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BloodDonorRepository extends JpaRepository<BloodDonor, Long> {

    List<BloodDonor> findAllByBloodType(BloodType bloodType);
}
