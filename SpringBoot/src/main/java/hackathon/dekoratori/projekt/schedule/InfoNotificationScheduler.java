package hackathon.dekoratori.projekt.schedule;

import hackathon.dekoratori.projekt.service.notification.InfoNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class InfoNotificationScheduler {

    private final InfoNotificationService infoNotificationService;

    @Autowired
    public InfoNotificationScheduler(final InfoNotificationService infoNotificationService) {
        this.infoNotificationService = infoNotificationService;
    }

    @EventListener(ContextRefreshedEvent.class)
    @Scheduled(cron = "0 0 18 * * *")
    public void syncUsers() {
        this.infoNotificationService.sendInfoNotifications();
    }
}
