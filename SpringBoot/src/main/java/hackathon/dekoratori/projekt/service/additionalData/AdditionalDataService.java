package hackathon.dekoratori.projekt.service.additionalData;

import hackathon.dekoratori.projekt.domain.additionalData.AdditionalData;

import java.util.List;

public interface AdditionalDataService {

    List<AdditionalData> getAllForUser(Long userId);

    List<AdditionalData> edit(List<AdditionalData> additionalData);
}
