package hackathon.dekoratori.projekt.service.additionalData;

import hackathon.dekoratori.projekt.domain.additionalData.AdditionalData;
import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import hackathon.dekoratori.projekt.repository.additionalData.AdditionalDataRepository;
import hackathon.dekoratori.projekt.repository.users.BloodDonorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdditionalDataServiceImpl implements AdditionalDataService {

    private final AdditionalDataRepository additionalDataRepository;

    private final BloodDonorRepository bloodDonorRepository;

    @Autowired
    public AdditionalDataServiceImpl(AdditionalDataRepository additionalDataRepository, BloodDonorRepository bloodDonorRepository) {
        this.additionalDataRepository = additionalDataRepository;
        this.bloodDonorRepository = bloodDonorRepository;
    }

    @Override
    public List<AdditionalData> getAllForUser(Long userId) {
        BloodDonor bloodDonor = this.bloodDonorRepository.findOne(userId);
        return this.additionalDataRepository.findAllByBloodDonor(bloodDonor);
    }

    @Override
    public List<AdditionalData> edit(List<AdditionalData> additionalData) {
        return this.additionalDataRepository.save(additionalData);
    }
}
