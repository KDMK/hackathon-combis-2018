package hackathon.dekoratori.projekt.service.algorithm;

import hackathon.dekoratori.projekt.controller.algorithm.AlgorithmDTO;

import java.util.List;

public interface AlgorithmService {

   AlgorithmDTO getData();
}
