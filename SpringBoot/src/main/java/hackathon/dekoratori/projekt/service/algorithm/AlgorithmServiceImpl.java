package hackathon.dekoratori.projekt.service.algorithm;

import hackathon.dekoratori.projekt.controller.algorithm.AlgorithmDTO;
import hackathon.dekoratori.projekt.domain.csv.CsvBloodData;
import hackathon.dekoratori.projekt.enumeration.Gender;
import hackathon.dekoratori.projekt.service.bloodSupply.BloodSupplyService;
import hackathon.dekoratori.projekt.service.csv.CSVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AlgorithmServiceImpl implements AlgorithmService {

	private final CSVService csvService;

	private final BloodSupplyService bloodSupplyService;

	@Autowired
	public AlgorithmServiceImpl(CSVService csvService, BloodSupplyService bloodSupplyService) {
		this.csvService = csvService;
		this.bloodSupplyService = bloodSupplyService;
	}

	@Override
	public AlgorithmDTO getData() {
		List<CsvBloodData> csvBloodData = this.csvService.getBloodData()
				.stream()
				.filter(record -> Gender.getByName(record.getSex()).getDonationPeriod() < record.getLast_donation() + 28)
				.collect(Collectors.toList());

		Map<String, Long> bloodSupplies = this.bloodSupplyService.getBloodSupplies();

		return new AlgorithmDTO(csvBloodData, bloodSupplies);
	}
}