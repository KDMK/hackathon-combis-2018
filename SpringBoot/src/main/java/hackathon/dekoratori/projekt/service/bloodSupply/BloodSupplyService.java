package hackathon.dekoratori.projekt.service.bloodSupply;

import hackathon.dekoratori.projekt.domain.bloodSupply.BloodSupply;

import java.util.List;
import java.util.Map;

public interface BloodSupplyService {

    List<BloodSupply> getAll();

    BloodSupply get(Long id);

    BloodSupply add(BloodSupply bloodSupply);

    BloodSupply edit(BloodSupply bloodSupply);

    void delete(Long id);

    Map<String, Long> getBloodSupplies();
}
