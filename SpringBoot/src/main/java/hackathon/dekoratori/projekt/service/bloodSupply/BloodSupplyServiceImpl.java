package hackathon.dekoratori.projekt.service.bloodSupply;

import hackathon.dekoratori.projekt.domain.bloodSupply.BloodSupply;
import hackathon.dekoratori.projekt.repository.bloodSupply.BloodSupplyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BloodSupplyServiceImpl implements BloodSupplyService {

    private final BloodSupplyRepository bloodSupplyRepository;

    @Autowired
    public BloodSupplyServiceImpl(BloodSupplyRepository bloodSupplyRepository) {
        this.bloodSupplyRepository = bloodSupplyRepository;
    }

    @Override
    public List<BloodSupply> getAll() {
        return this.bloodSupplyRepository.findAll();
    }

    @Override
    public BloodSupply get(Long id) {
        return this.bloodSupplyRepository.findOne(id);
    }

    @Override
    public BloodSupply add(BloodSupply bloodSupply) {
        return this.bloodSupplyRepository.save(bloodSupply);
    }

    @Override
    public BloodSupply edit(BloodSupply bloodSupply) {
        return this.bloodSupplyRepository.save(bloodSupply);
    }

    @Override
    public void delete(Long id) {
        this.bloodSupplyRepository.delete(id);
    }

    @Override
    public Map<String, Long> getBloodSupplies() {
        return this.bloodSupplyRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(bloodSupply -> bloodSupply.getBloodType().toString(), BloodSupply::getAmount));
    }
}
