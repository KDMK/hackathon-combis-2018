package hackathon.dekoratori.projekt.service.csv;

import hackathon.dekoratori.projekt.domain.csv.CsvBloodData;

import java.util.List;

public interface CSVService {

    public List<CsvBloodData> getBloodData();
}
