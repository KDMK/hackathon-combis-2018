package hackathon.dekoratori.projekt.service.csv;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import hackathon.dekoratori.projekt.domain.csv.CsvBloodData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CSVServiceImpl implements CSVService{

    private static final String DONORS_CSV = "http://hackaton.westeurope.cloudapp.azure.com/get_donors";

    private final RestTemplate restTemplate;

    @Autowired
    public CSVServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<CsvBloodData> getBloodData() {
        String bla = restTemplate.getForObject(DONORS_CSV, String.class);

        CsvToBean<CsvBloodData> csvToBean = new CsvToBean<>();

        Map<String, String> columnMapping = new HashMap<>();
        columnMapping.put("id", "id");
        columnMapping.put("last_donation", "last_donation");
        columnMapping.put("frequency", "frequency");
        columnMapping.put("blood_group", "blood_group");
        columnMapping.put("Distance", "distance");
        columnMapping.put("Sex", "sex");

        HeaderColumnNameTranslateMappingStrategy<CsvBloodData> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(CsvBloodData.class);
        strategy.setColumnMapping(columnMapping);

        List<CsvBloodData> list;
        CSVReader reader = new CSVReader(new StringReader(bla));
        list = csvToBean.parse(strategy, reader);

        return list;
    }
}
