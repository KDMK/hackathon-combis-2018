package hackathon.dekoratori.projekt.service.notification;

import hackathon.dekoratori.projekt.domain.notification.DonationRequestNotification;
import hackathon.dekoratori.projekt.domain.users.BloodDonor;

import java.util.List;

public interface DonationRequestNotificationService {

    List<DonationRequestNotification> getAll();

    DonationRequestNotification get(Long id);

    DonationRequestNotification add(DonationRequestNotification donationRequestNotification);

    DonationRequestNotification edit(DonationRequestNotification donationRequestNotification);

    void delete(Long id);

    List<DonationRequestNotification> addNotifications(List<BloodDonor> bloodDonors);
}
