package hackathon.dekoratori.projekt.service.notification;

import hackathon.dekoratori.projekt.domain.notification.DonationRequestNotification;
import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import hackathon.dekoratori.projekt.enumeration.DonationRequestStatus;
import hackathon.dekoratori.projekt.repository.notification.DonationRequestNotificationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DonationRequestNotificationServiceImpl implements DonationRequestNotificationService {

    private final DonationRequestNotificationRepository donationRequestNotificationRepository;

    @Autowired
    public DonationRequestNotificationServiceImpl(DonationRequestNotificationRepository donationRequestNotificationRepository) {
        this.donationRequestNotificationRepository = donationRequestNotificationRepository;
    }

    @Override
    public List<DonationRequestNotification> getAll() {
        return this.donationRequestNotificationRepository.findAll();
    }

    @Override
    public DonationRequestNotification get(Long id) {
        return this.donationRequestNotificationRepository.findOne(id);
    }

    @Override
    public DonationRequestNotification add(DonationRequestNotification donationRequestNotification) {
        return this.donationRequestNotificationRepository.save(donationRequestNotification);
    }

    @Override
    public DonationRequestNotification edit(DonationRequestNotification donationRequestNotification) {
        return this.donationRequestNotificationRepository.save(donationRequestNotification);
    }

    @Override
    public void delete(Long id) {
        this.donationRequestNotificationRepository.delete(id);
    }

    @Override
    public List<DonationRequestNotification> addNotifications(List<BloodDonor> bloodDonors) {
        List<DonationRequestNotification> donationRequestNotifications = new ArrayList<>();

        for(BloodDonor bloodDonor : bloodDonors) {
            donationRequestNotifications.add(
                    new DonationRequestNotification(
                        bloodDonor.getId(),
                        "Poziv na darivanje krvi",
                        "Poštovani, pozivamo Vas da darujete krv i spasite nekome život!",
                        DonationRequestStatus.NOT_RESPONDED
                    )
            );
        }

        return donationRequestNotifications;
    }
}
