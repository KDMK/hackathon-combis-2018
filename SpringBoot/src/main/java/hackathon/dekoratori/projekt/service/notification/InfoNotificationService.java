package hackathon.dekoratori.projekt.service.notification;

import hackathon.dekoratori.projekt.domain.notification.InfoNotification;

import java.util.List;

public interface InfoNotificationService {

    List<InfoNotification> getAll();

    InfoNotification get(Long id);

    InfoNotification add(InfoNotification infoNotification);

    InfoNotification edit(InfoNotification infoNotification);

    void delete(Long id);

    void sendInfoNotifications();
}
