package hackathon.dekoratori.projekt.service.notification;

import hackathon.dekoratori.projekt.domain.notification.DonationRequestNotification;
import hackathon.dekoratori.projekt.domain.notification.InfoNotification;
import hackathon.dekoratori.projekt.enumeration.DonationRequestStatus;
import hackathon.dekoratori.projekt.repository.notification.InfoNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InfoNotificationServiceImpl implements InfoNotificationService {

	private final InfoNotificationRepository infoNotificationRepository;
	@Autowired
	private DonationRequestNotificationService donationRequestNotificationService;

	@Autowired
	public InfoNotificationServiceImpl(InfoNotificationRepository infoNotificationRepository) {
		this.infoNotificationRepository = infoNotificationRepository;
	}

	@Override
	public List<InfoNotification> getAll() {
		return this.infoNotificationRepository.findAll();
	}

	@Override
	public InfoNotification get(Long id) {
		return this.infoNotificationRepository.findOne(id);
	}

	@Override
	public InfoNotification add(InfoNotification infoNotification) {
		return this.infoNotificationRepository.save(infoNotification);
	}

	@Override
	public InfoNotification edit(InfoNotification infoNotification) {
		return this.infoNotificationRepository.save(infoNotification);
	}

	@Override
	public void delete(Long id) {
		this.infoNotificationRepository.delete(id);
	}

	@Override
	public void sendInfoNotifications() {
		List<DonationRequestNotification> allDonationRequest = donationRequestNotificationService.getAll();
		for (DonationRequestNotification donationRequestNotification : allDonationRequest) {
			if (donationRequestNotification.getDonationRequestStatus() == DonationRequestStatus.ACCEPTED) {

			}
		}
	}
}
