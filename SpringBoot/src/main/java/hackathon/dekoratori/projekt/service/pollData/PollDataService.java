package hackathon.dekoratori.projekt.service.pollData;

import hackathon.dekoratori.projekt.domain.pollData.PollData;

import java.util.List;

public interface PollDataService {

    List<PollData> getAll();

    PollData get(Long id);

    PollData add(PollData pollData);

    PollData edit(PollData pollData);

    void delete(Long id);
}
