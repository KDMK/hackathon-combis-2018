package hackathon.dekoratori.projekt.service.pollData;

import hackathon.dekoratori.projekt.domain.pollData.PollData;
import hackathon.dekoratori.projekt.repository.pollData.PollDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PollDataServiceImpl implements PollDataService {

    private final PollDataRepository pollDataRepository;

    @Autowired
    public PollDataServiceImpl(PollDataRepository pollDataRepository) {
        this.pollDataRepository = pollDataRepository;
    }

    @Override
    public List<PollData> getAll() {
        return this.pollDataRepository.findAll();
    }

    @Override
    public PollData get(Long id) {
        return this.pollDataRepository.findOne(id);
    }

    @Override
    public PollData add(PollData pollData) {
        return this.pollDataRepository.save(pollData);
    }

    @Override
    public PollData edit(PollData pollData) {
        return this.pollDataRepository.save(pollData);
    }

    @Override
    public void delete(Long id) {
        this.pollDataRepository.delete(id);
    }
}
