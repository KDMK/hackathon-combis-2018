package hackathon.dekoratori.projekt.service.users;

import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import hackathon.dekoratori.projekt.enumeration.BloodType;

import java.util.List;

public interface BloodDonorService {

    List<BloodDonor> getAll();

    BloodDonor get(Long id);

    BloodDonor add(BloodDonor bloodDonor);

    BloodDonor edit(BloodDonor bloodDonor);

    void delete(Long id);

    void sendDonationRequests(Long doses, BloodType bloodType);
}
