package hackathon.dekoratori.projekt.service.users;

import hackathon.dekoratori.projekt.domain.additionalData.AdditionalData;
import hackathon.dekoratori.projekt.domain.algorithm.AlgorithmRequest;
import hackathon.dekoratori.projekt.domain.notification.DonationRequestNotification;
import hackathon.dekoratori.projekt.domain.users.BloodDonor;
import hackathon.dekoratori.projekt.enumeration.BloodType;
import hackathon.dekoratori.projekt.enumeration.Gender;
import hackathon.dekoratori.projekt.repository.users.BloodDonorRepository;
import hackathon.dekoratori.projekt.service.notification.DonationRequestNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BloodDonorServiceImpl implements BloodDonorService {

    private static final String ALGORITHM_URL = "http://localhost:8090/api/algorithm";

    private final RestTemplate restTemplate;

    private final BloodDonorRepository bloodDonorRepository;

    private final DonationRequestNotificationService donationRequestNotificationService;

    @Autowired
    public BloodDonorServiceImpl(BloodDonorRepository bloodDonorRepository, RestTemplate restTemplate, DonationRequestNotificationService donationRequestNotificationService) {
        this.bloodDonorRepository = bloodDonorRepository;
        this.restTemplate = restTemplate;
        this.donationRequestNotificationService = donationRequestNotificationService;
    }

    @Override
    public List<BloodDonor> getAll() {
        return this.bloodDonorRepository.findAll();
    }

    @Override
    public BloodDonor get(Long id) {
        return this.bloodDonorRepository.findOne(id);
    }

    @Override
    public BloodDonor add(BloodDonor bloodDonor) {
        return this.bloodDonorRepository.save(bloodDonor);
    }

    @Override
    public BloodDonor edit(BloodDonor bloodDonor) {
        return this.bloodDonorRepository.save(bloodDonor);
    }

    @Override
    public void delete(Long id) {
        this.bloodDonorRepository.delete(id);
    }

    @Override
    public void sendDonationRequests(Long doses, BloodType bloodType) {
        List<BloodDonor> potentialDonors = this.bloodDonorRepository.findAllByBloodType(bloodType)
                .stream()
                .filter(this::enoughTimeHasPassed)
                .filter(this::criticalConditionIsNotViolated)
                .collect(Collectors.toList());

        ResponseEntity<List<BloodDonor>> responseEntity = getAlgorithmResponse(potentialDonors, doses);
        this.donationRequestNotificationService.addNotifications(responseEntity.getBody());
    }

    private ResponseEntity<List<BloodDonor>> getAlgorithmResponse(List<BloodDonor> potentialDonors, Long doses) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AlgorithmRequest> entity = new HttpEntity<>(new AlgorithmRequest(potentialDonors, doses), headers);
        return restTemplate.exchange(ALGORITHM_URL, HttpMethod.POST, entity, new ParameterizedTypeReference<List<BloodDonor>>(){});
    }

    private Boolean enoughTimeHasPassed(BloodDonor bloodDonor) {
        LocalDate lastDonationDate = bloodDonor.getLastDonation();
        Long donationPeriod = Gender.getByName(bloodDonor.getGender().toString()).getDonationPeriod();

        return LocalDate.now().minusDays(donationPeriod).isAfter(lastDonationDate);
    }

    private Boolean criticalConditionIsNotViolated(BloodDonor bloodDonor) {
        Set<AdditionalData> additionalData = bloodDonor.getAdditionalData();

        for(AdditionalData additionalDataItem : additionalData) {
            if(isCriticalConditionViolatedForItem(additionalDataItem)) {
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    private boolean isCriticalConditionViolatedForItem(AdditionalData additionalDataItem) {
        return additionalDataItem.getPollData().getIsCritical() && additionalDataItem.getValue();
    }
}
