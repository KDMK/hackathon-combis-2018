package hackathon.dekoratori.projekt.service.users;

import hackathon.dekoratori.projekt.domain.users.Users;

import java.util.List;

public interface UserService {

    List<Users> getAll();

    Users get(Long id);

    Users add(Users user);

    Users edit(Users user);

    void delete(Long id);
}
