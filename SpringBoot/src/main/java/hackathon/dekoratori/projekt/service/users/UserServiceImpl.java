package hackathon.dekoratori.projekt.service.users;

import hackathon.dekoratori.projekt.domain.users.Users;
import hackathon.dekoratori.projekt.repository.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<Users> getAll() {
        return this.userRepository.findAll();
    }

    @Override
    public Users get(Long id) {
        return this.userRepository.findOne(id);
    }

    @Override
    public Users add(Users user) {
        return this.userRepository.save(user);
    }

    @Override
    public Users edit(Users user) {
        return this.userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        this.userRepository.delete(id);
    }
}
