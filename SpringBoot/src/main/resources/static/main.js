(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _component_app_app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./component/app/app.component */ "./src/app/component/app/app.component.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./material.module */ "./src/app/material.module.ts");
/* harmony import */ var _service_global_error_handler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/global-error-handler.service */ "./src/app/service/global-error-handler.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _service_app_router_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service/app-router.module */ "./src/app/service/app-router.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular5-toaster/dist */ "./node_modules/angular5-toaster/dist/angular5-toaster.js");
/* harmony import */ var _component_home_home_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./component/home/home.component */ "./src/app/component/home/home.component.ts");
/* harmony import */ var _component_bloodSupply_edit_bloodSupply_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./component/bloodSupply/edit-bloodSupply.component */ "./src/app/component/bloodSupply/edit-bloodSupply.component.ts");
/* harmony import */ var _component_bloodSupply_bloodSupply_new_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./component/bloodSupply/bloodSupply-new.dialog */ "./src/app/component/bloodSupply/bloodSupply-new.dialog.ts");
/* harmony import */ var _component_donor_profile_donor_profile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./component/donor-profile/donor-profile.component */ "./src/app/component/donor-profile/donor-profile.component.ts");
/* harmony import */ var _component_bloodDonor_blood_donor_table_blood_donor_table_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./component/bloodDonor/blood-donor-table/blood-donor-table.component */ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.ts");
/* harmony import */ var _component_notification_notification_notification_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./component/notification/notification/notification.component */ "./src/app/component/notification/notification/notification.component.ts");
/* harmony import */ var _component_poll_poll_poll_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./component/poll/poll/poll.component */ "./src/app/component/poll/poll/poll.component.ts");
/* harmony import */ var _component_additionalData_additional_data_additional_data_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./component/additionalData/additional-data/additional-data.component */ "./src/app/component/additionalData/additional-data/additional-data.component.ts");
/* harmony import */ var _component_poll_poll_poll_new_dialog__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./component/poll/poll/poll-new.dialog */ "./src/app/component/poll/poll/poll-new.dialog.ts");
/* harmony import */ var angularx_qrcode__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! angularx-qrcode */ "./node_modules/angularx-qrcode/dist/index.js");
/* harmony import */ var _component_qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./component/qrcode/qrcode.component */ "./src/app/component/qrcode/qrcode.component.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./service/auth.service */ "./src/app/service/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _component_app_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _component_home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"],
                _component_bloodSupply_edit_bloodSupply_component__WEBPACK_IMPORTED_MODULE_10__["BloodSupplyComponent"],
                _component_bloodSupply_bloodSupply_new_dialog__WEBPACK_IMPORTED_MODULE_11__["BloodSupplyNewDialog"],
                _component_donor_profile_donor_profile_component__WEBPACK_IMPORTED_MODULE_12__["DonorProfileComponent"],
                _component_bloodSupply_bloodSupply_new_dialog__WEBPACK_IMPORTED_MODULE_11__["BloodSupplyNewDialog"],
                _component_bloodDonor_blood_donor_table_blood_donor_table_component__WEBPACK_IMPORTED_MODULE_13__["BloodDonorTableComponent"],
                _component_notification_notification_notification_component__WEBPACK_IMPORTED_MODULE_14__["NotificationComponent"],
                _component_poll_poll_poll_component__WEBPACK_IMPORTED_MODULE_15__["PollComponent"],
                _component_poll_poll_poll_new_dialog__WEBPACK_IMPORTED_MODULE_17__["PollNewDialog"],
                _component_additionalData_additional_data_additional_data_component__WEBPACK_IMPORTED_MODULE_16__["AdditionalDataComponent"],
                _component_qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_19__["QrcodeComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_3__["MaterialModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _service_app_router_module__WEBPACK_IMPORTED_MODULE_6__["AppRouterModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_8__["ToasterModule"],
                angularx_qrcode__WEBPACK_IMPORTED_MODULE_18__["QRCodeModule"]
            ],
            providers: [
                _service_auth_service__WEBPACK_IMPORTED_MODULE_20__["AuthService"],
                {
                    provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ErrorHandler"],
                    useClass: _service_global_error_handler_service__WEBPACK_IMPORTED_MODULE_4__["GlobalErrorHandlerService"]
                }
            ],
            entryComponents: [
                _component_bloodSupply_bloodSupply_new_dialog__WEBPACK_IMPORTED_MODULE_11__["BloodSupplyNewDialog"],
                _component_poll_poll_poll_new_dialog__WEBPACK_IMPORTED_MODULE_17__["PollNewDialog"]
            ],
            bootstrap: [_component_app_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/component/additionalData/additional-data/additional-data.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/component/additionalData/additional-data/additional-data.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/component/additionalData/additional-data/additional-data.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/component/additionalData/additional-data/additional-data.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  additional-data works!\n</p>\n"

/***/ }),

/***/ "./src/app/component/additionalData/additional-data/additional-data.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/component/additionalData/additional-data/additional-data.component.ts ***!
  \***************************************************************************************/
/*! exports provided: AdditionalDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdditionalDataComponent", function() { return AdditionalDataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_additional_data_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../service/additional-data-service.service */ "./src/app/service/additional-data-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AdditionalDataComponent = /** @class */ (function () {
    function AdditionalDataComponent(additionalDataService) {
        this.additionalDataService = additionalDataService;
    }
    AdditionalDataComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    AdditionalDataComponent.prototype.loadData = function () {
        var _this = this;
        this.additionalDataService.getAdditionalDataForDonor(1).toPromise()
            .then(function (response) {
            _this.additionalData = response.body;
        }).catch(function (err) {
            console.error(err);
        });
    };
    AdditionalDataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-additional-data',
            template: __webpack_require__(/*! ./additional-data.component.html */ "./src/app/component/additionalData/additional-data/additional-data.component.html"),
            styles: [__webpack_require__(/*! ./additional-data.component.css */ "./src/app/component/additionalData/additional-data/additional-data.component.css")]
        }),
        __metadata("design:paramtypes", [_service_additional_data_service_service__WEBPACK_IMPORTED_MODULE_1__["AdditionalDataServiceService"]])
    ], AdditionalDataComponent);
    return AdditionalDataComponent;
}());



/***/ }),

/***/ "./src/app/component/app/app.component.css":
/*!*************************************************!*\
  !*** ./src/app/component/app/app.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-container {\n  height: calc(100vh - 45px);\n}\n\n.side-container {\n  background-color: #D80027;\n  width: 100%;\n  height: 45px\n}\n\n.progress-bar {\n  position: fixed;\n  top: 0;\n  left: 0;\n  height: 7px\n}\n\n.content-container {\n  max-width: 1200px;\n  margin: auto;\n  padding-left: 2em;\n  padding-right: 2em\n}\n\n.data-button {\n  display: block;\n  width: 15em\n}\n"

/***/ }),

/***/ "./src/app/component/app/app.component.html":
/*!**************************************************!*\
  !*** ./src/app/component/app/app.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-progress-bar class=\"progress-bar\" mode=\"query\" *ngIf=\"isLoading | async\"></mat-progress-bar>\n\n<toaster-container [toasterconfig]=\"toasterconfig\"></toaster-container>\n\n<div class=\"side-container\">\n  <button (click)=\"sidenav.toggle()\" mat-icon-button>\n    <mat-icon>menu</mat-icon>\n  </button>\n</div>\n\n<mat-drawer-container class=\"main-container\">\n  <mat-drawer #sidenav mode=\"over\">\n    <button mat-button class=\"data-button\" [routerLink]=\"['']\">Početna</button>\n    <button mat-button class=\"data-button\" [routerLink]=\"['notifications']\">Notifikacije</button>\n    <button mat-button class=\"data-button\" [routerLink]=\"['polls']\">Anketa</button>\n    <button mat-button class=\"data-button\" [routerLink]=\"['blood-supplies']\">Zalihe krvi</button>\n    <button mat-button class=\"data-button\" [routerLink]=\"['users']\">Pregled donora</button>\n    <button mat-button class=\"data-button\" [routerLink]=\"['qr-generator']\">Generiraj QR potvrdu</button>\n  </mat-drawer>\n\n  <div class=\"content-container\">\n    <router-outlet></router-outlet>\n  </div>\n</mat-drawer-container>\n"

/***/ }),

/***/ "./src/app/component/app/app.component.ts":
/*!************************************************!*\
  !*** ./src/app/component/app/app.component.ts ***!
  \************************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angular5-toaster/dist */ "./node_modules/angular5-toaster/dist/angular5-toaster.js");
/* harmony import */ var _service_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/loader.service */ "./src/app/service/loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/auth.service */ "./src/app/service/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = /** @class */ (function () {
    function AppComponent(loaderService, toaster, router, auth) {
        this.loaderService = loaderService;
        this.toaster = toaster;
        this.router = router;
        this.auth = auth;
        this.toasterconfig = new angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_1__["ToasterConfig"]({
            showCloseButton: true,
            tapToDismiss: false,
            timeout: 0
        });
        this.isLoading = this.loaderService.getLoadingState();
        auth.getLoggedInUser();
    }
    AppComponent.prototype.popToast = function () {
        this.toaster.pop("success", "Uspjeh!", "Test notifikacije");
    };
    AppComponent.prototype.btnUsers = function () {
        this.router.navigateByUrl("/users");
    };
    AppComponent.prototype.btnNotifications = function () {
        this.router.navigateByUrl("/notifications");
    };
    AppComponent.prototype.btnPolls = function () {
        this.router.navigateByUrl("/polls");
    };
    AppComponent.prototype.btnAdditionalData = function () {
        this.router.navigateByUrl("/additional-data");
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/component/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/component/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_service_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"], angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_1__["ToasterService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-container {\n  margin-top: 2em;\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px;\n  overflow: auto\n}\n\n.mat-row:hover {\n  background-color: whitesmoke;\n  cursor: pointer;\n}\n"

/***/ }),

/***/ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-container\">\n  <mat-toolbar class=\"mat-elevation-z1\">Popis Donora&nbsp;&nbsp;<button mat-icon-button (click)=\"openNewDialog()\">\n    <mat-icon>add</mat-icon>\n  </button>\n  </mat-toolbar>\n  <table mat-table matSort class=\"mat-elevation-z8\" [dataSource]=\"dataSource\">\n    <ng-container matColumnDef=\"name\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Name</th>\n      <td mat-cell *matCellDef=\"let donor\">{{donor.lastName + \" \" + donor.firstName}}</td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"bloodType\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Blood type</th>\n      <td mat-cell *matCellDef=\"let donor\">{{donor.bloodType}}</td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"gender\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Gender</th>\n      <td mat-cell *matCellDef=\"let donor\">{{donor.gender}}</td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"canDonate\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Can donate</th>\n      <td mat-cell *matCellDef=\"let donor\">{{canDonate(donor)}}</td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row (click)=\"openDonorProfile(row.id)\" *matRowDef=\"let row; columns: displayedColumns\"></tr>\n  </table>\n</div>\n"

/***/ }),

/***/ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.ts ***!
  \***************************************************************************************/
/*! exports provided: BloodDonorTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloodDonorTableComponent", function() { return BloodDonorTableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_bloodDonor_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../service/bloodDonor.service */ "./src/app/service/bloodDonor.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BloodDonorTableComponent = /** @class */ (function () {
    function BloodDonorTableComponent(bloodDonorService, router) {
        this.bloodDonorService = bloodDonorService;
        this.router = router;
        this.displayedColumns = [
            "name",
            "bloodType",
            "gender",
            "canDonate"
        ];
    }
    BloodDonorTableComponent.prototype.ngOnInit = function () {
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.donors);
        this.loadData();
    };
    BloodDonorTableComponent.prototype.canDonate = function (donor) {
        var lastDonated = new Date(donor.lastDonation);
        var daysBetweenDonations = (donor.gender == "M") ? 90 : 120;
        if (this.inBetween(lastDonated, new Date()) > daysBetweenDonations) {
            return "YES";
        }
        return "NO";
    };
    BloodDonorTableComponent.prototype.openDonorProfile = function (id) {
        this.router.navigate(['donor-profile']);
    };
    BloodDonorTableComponent.prototype.inBetween = function (date1, date2) {
        //Get 1 day in milliseconds
        var one_day = 1000 * 60 * 60 * 24;
        // Convert both dates to milliseconds
        var date1_ms = date1.getTime();
        var date2_ms = date2.getTime();
        // Calculate the difference in milliseconds
        var difference_ms = date2_ms - date1_ms;
        // Convert back to days and return
        return Math.round(difference_ms / one_day);
    };
    BloodDonorTableComponent.prototype.loadData = function () {
        var _this = this;
        this.bloodDonorService.getAllDonors().toPromise()
            .then(function (response) {
            _this.donors = response.body;
            _this.dataSource.data = _this.donors;
        }).catch(function (err) {
            console.error(err);
        });
    };
    BloodDonorTableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blood-donor-table',
            template: __webpack_require__(/*! ./blood-donor-table.component.html */ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.html"),
            styles: [__webpack_require__(/*! ./blood-donor-table.component.css */ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.css")]
        }),
        __metadata("design:paramtypes", [_service_bloodDonor_service__WEBPACK_IMPORTED_MODULE_1__["BloodDonorService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], BloodDonorTableComponent);
    return BloodDonorTableComponent;
}());



/***/ }),

/***/ "./src/app/component/bloodSupply/bloodSupply-new.dialog.html":
/*!*******************************************************************!*\
  !*** ./src/app/component/bloodSupply/bloodSupply-new.dialog.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div mat-dialog-title>\n    <h1 mat-dialog-title>{{data.title}}</h1>\n  </div>\n  <div mat-dialog-content>\n    <form>\n      <mat-form-field class=\"dialog-input\" appearance=\"standard\">\n        <mat-label>Kolicina</mat-label>\n        <input type=\"number\" min=\"1\" matInput [(ngModel)]=\"bloodRequest.amount\" placeholder=\"Unesite kolicnu...\" name=\"amount\">\n      </mat-form-field>\n\n      <mat-form-field class=\"dialog-input\" appearance=\"standard\">\n        <mat-label>Krvna grupa</mat-label>\n        <mat-select [(ngModel)]=\"bloodRequest.bloodType\" placeholder=\"Odaberite krvnu grupu\" name=\"bloodType\">\n          <mat-option [value]=\"'OM'\">O-</mat-option>\n          <mat-option [value]=\"'AM'\">A-</mat-option>\n          <mat-option [value]=\"'BM'\">B-</mat-option>\n          <mat-option [value]=\"'ABM'\">AB-</mat-option>\n          <mat-option [value]=\"'OP'\">O-</mat-option>\n          <mat-option [value]=\"'AP'\">A-</mat-option>\n          <mat-option [value]=\"'BP'\">B-</mat-option>\n          <mat-option [value]=\"'ABP'\">AB-</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </form>\n  </div>\n  <div mat-dialog-actions class=\"dialog-footer\">\n    <button mat-button (click)=\"closeDialog()\">Odustani</button>\n    <button mat-button [mat-dialog-close]=\"bloodRequest\" cdkFocusInitial>Spremi</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/bloodSupply/bloodSupply-new.dialog.ts":
/*!*****************************************************************!*\
  !*** ./src/app/component/bloodSupply/bloodSupply-new.dialog.ts ***!
  \*****************************************************************/
/*! exports provided: BloodSupplyNewDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloodSupplyNewDialog", function() { return BloodSupplyNewDialog; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var BloodSupplyNewDialog = /** @class */ (function () {
    function BloodSupplyNewDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.bloodRequest = Object.assign({}, data.category);
        console.log(data);
    }
    BloodSupplyNewDialog.prototype.closeDialog = function () {
        this.dialogRef.close(null);
    };
    BloodSupplyNewDialog = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "dialog-blood-supply-new",
            template: __webpack_require__(/*! ./bloodSupply-new.dialog.html */ "./src/app/component/bloodSupply/bloodSupply-new.dialog.html"),
            styles: [__webpack_require__(/*! ./edit-bloodSupply.component.css */ "./src/app/component/bloodSupply/edit-bloodSupply.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], BloodSupplyNewDialog);
    return BloodSupplyNewDialog;
}());



/***/ }),

/***/ "./src/app/component/bloodSupply/edit-bloodSupply.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/component/bloodSupply/edit-bloodSupply.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-container {\n  margin-top: 2em;\n  display: flex;\n  flex-direction: column;\n  /*max-height: 500px;*/\n  min-width: 300px;\n  overflow: auto;\n}\n\n.dialog-input {\n  width: 100%;\n}\n\n.dialog-footer {\n  margin: 1.25em 0 0.5em auto;\n  float: right\n}\n\n.example-header {\n  background-color: white;\n  min-height: 64px;\n  padding: 8px 24px 0;\n}\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%;\n}\n"

/***/ }),

/***/ "./src/app/component/bloodSupply/edit-bloodSupply.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/component/bloodSupply/edit-bloodSupply.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-container mat-elevation-z1\">\n  <mat-toolbar class=\"mat-elevation-z1\">Zalihe krvi&nbsp;&nbsp;<button mat-icon-button\n                                                                       (click)=\"openNewDialog()\">\n    <mat-icon>add</mat-icon>\n  </button>\n  </mat-toolbar>\n  <div class=\"example-header mat-elevation-z1\">\n    <mat-form-field>\n      <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filtriraj krvne grupe...\">\n    </mat-form-field>\n  </div>\n  <table mat-table matSort #bloodTypeTable [dataSource]=\"dataSource\">\n    <ng-container matColumnDef=\"bloodType\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Krvna grupa</th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.bloodType}}</td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"amount\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Preostala količina krvi</th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.amount}}</td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\n  </table>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/component/bloodSupply/edit-bloodSupply.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/component/bloodSupply/edit-bloodSupply.component.ts ***!
  \*********************************************************************/
/*! exports provided: BloodSupplyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloodSupplyComponent", function() { return BloodSupplyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _bloodSupply_new_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bloodSupply-new.dialog */ "./src/app/component/bloodSupply/bloodSupply-new.dialog.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular5-toaster/dist */ "./node_modules/angular5-toaster/dist/angular5-toaster.js");
/* harmony import */ var _service_bloodSupply_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/bloodSupply.service */ "./src/app/service/bloodSupply.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BloodSupplyComponent = /** @class */ (function () {
    function BloodSupplyComponent(bloodSupplyService, dialog, toasterService) {
        this.bloodSupplyService = bloodSupplyService;
        this.dialog = dialog;
        this.toasterService = toasterService;
        this.BLOOD_REQUEST = "Pošalji zahtjev za prikupljanje krvi";
        this.displayedColumns = ['bloodType', 'amount'];
    }
    BloodSupplyComponent.prototype.ngOnInit = function () {
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](this.bloodSupplies);
        this.dataSource.sort = this.sort;
        this.refreshBloodSupplies();
    };
    BloodSupplyComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    BloodSupplyComponent.prototype.refreshBloodSupplies = function () {
        var _this = this;
        this.bloodSupplyService.getAllBloodSupply().toPromise().then(function (response) {
            _this.bloodSupplies = response.body;
            _this.bloodSupplies.forEach(function (t) {
                if (t.bloodType.indexOf('P') != -1) {
                    t.bloodType = t.bloodType.replace('P', '+');
                }
                else {
                    t.bloodType = t.bloodType.replace('M', '-');
                }
            });
            _this.dataSource.data = _this.bloodSupplies;
        }).catch(function (err) {
            console.error(err);
        });
    };
    BloodSupplyComponent.prototype.openNewDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_bloodSupply_new_dialog__WEBPACK_IMPORTED_MODULE_1__["BloodSupplyNewDialog"], {
            width: "500px",
            data: {
                title: this.BLOOD_REQUEST,
            },
            disableClose: true
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result == null || Object.keys(result).length == 0)
                return;
            _this.bloodSupplyService.bloodSupplyRequest(result).toPromise().then(function (response) {
                _this.toasterService.pop("success", "Uspješno je", "predan zahtjev za prikupljanjem krvi");
                _this.refreshBloodSupplies();
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], BloodSupplyComponent.prototype, "sort", void 0);
    BloodSupplyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-blood-supply',
            template: __webpack_require__(/*! ./edit-bloodSupply.component.html */ "./src/app/component/bloodSupply/edit-bloodSupply.component.html"),
            styles: [__webpack_require__(/*! ./edit-bloodSupply.component.css */ "./src/app/component/bloodSupply/edit-bloodSupply.component.css")]
        }),
        __metadata("design:paramtypes", [_service_bloodSupply_service__WEBPACK_IMPORTED_MODULE_4__["BloodSupplyService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"], angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_3__["ToasterService"]])
    ], BloodSupplyComponent);
    return BloodSupplyComponent;
}());



/***/ }),

/***/ "./src/app/component/donor-profile/donor-profile.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/component/donor-profile/donor-profile.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".profile-container {\n  padding: 1em;\n}\n\n.profile-info-row {\n  display: flex;\n  flex-wrap: wrap;\n  flex-direction: row;\n}\n\n.profile-info-img {\n  -ms-grid-row-align: center;\n      align-self: center;\n  margin-left: auto;\n  margin-right: auto;\n  flex: 1 0 25%;\n}\n\n.profile-info-img img {\n  width: calc(100% - 3em);\n  margin-left: 1.5em;\n  margin-right: 1.5em;\n}\n\n.profile-info-data {\n  min-width: 530px;\n  flex: 1 0 75%;\n}\n\n.profile-donation-list-row {\n  clear: both;\n  margin-top: 3em;\n  width: 100%;\n}\n\n.profile-donation-list {\n  width: 100%;\n}\n\n.profile-info-data-name {\n  font-size: 14pt;\n  font-weight: bold;\n  margin-bottom: 12px;\n}\n\n.profile-info-data-name {\n  font-size: 14pt;\n  font-weight: bold;\n  margin-bottom: 12px;\n}\n\n.profile-info-data-field {\n  margin-top: 0.25em;\n  margin-bottom: 0.25em;\n}\n\n@media(max-width: 834px) {\n  .profile-info-img img {\n    width: 250px;\n    margin-left: calc(50% - 125px);\n    margin-right: 5%;\n    margin-bottom: 1em;\n  }\n\n  .profile-info-data {\n    text-align: center;\n    min-width: 200px;\n    width: 350px;\n  }\n}\n"

/***/ }),

/***/ "./src/app/component/donor-profile/donor-profile.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/component/donor-profile/donor-profile.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"profile-container\">\n  <div class=\"profile-info-row\">\n    <div class=\"profile-info-img\">\n      <img src=\"/assets/avatar.png\"/>\n    </div>\n    <div class=\"profile-info-data\">\n      <div class=\"profile-info-data-name\">Ivan Horvat</div>\n      <div class=\"profile-info-data-field\">Krvna grupa: <span>AB+</span></div>\n      <div class=\"profile-info-data-field\">Datum zadnje donacije: <span>25.01.2018.</span></div>\n      <div>\n        <div class=\"profile-info-data-field\">Dodatne napomene:</div>\n        <div>\n          <div style=\"font-size: 8pt; margin-top: .25em; font-style: italic\">Boravio u Engleskoj za vrijeme epidemije gripe</div>\n          <div style=\"font-size: 8pt; margin-top: .25em; font-style: italic\">Povišena tijelesna težina prilikom zadnje kontrole prije vadenja krvi</div>\n        </div>\n    </div>\n  </div>\n</div>\n  <div class=\"profile-donation-list-row\">\n    <table class=\"profile-donation-list-row\" mat-table matSort #bloodDonationTable [dataSource]=\"dataSource\">\n      <ng-container matColumnDef=\"ord\">\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>No.</th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.ord}}</td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"date\">\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Datum</th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.date | date : \"shortDate\"}}</td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"location\">\n        <th mat-header-cell *matHeaderCellDef mat-sort-header>Lokacija donacije</th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.location}}</td>\n      </ng-container>\n\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\n    </table>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/donor-profile/donor-profile.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/component/donor-profile/donor-profile.component.ts ***!
  \********************************************************************/
/*! exports provided: DonorProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DonorProfileComponent", function() { return DonorProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular5-toaster/dist */ "./node_modules/angular5-toaster/dist/angular5-toaster.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DonorProfileComponent = /** @class */ (function () {
    function DonorProfileComponent(
    // private bloodDonorService: BloodDonorService,
    dialog, toasterService) {
        this.dialog = dialog;
        this.toasterService = toasterService;
        this.displayedColumns = ['ord', 'date', 'location'];
    }
    DonorProfileComponent.prototype.ngOnInit = function () {
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](this.bloodDonations);
        this.dataSource.sort = this.sort;
        this.refreshBloodSupplies();
    };
    DonorProfileComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    DonorProfileComponent.prototype.refreshBloodSupplies = function () {
        var data = [
            {
                "ord": 1,
                "date": new Date(2018, 4, 12),
                "location": "Petrova 3"
            },
            {
                "ord": 2,
                "date": new Date(2018, 1, 12),
                "location": "Petrova 3"
            },
            {
                "ord": 3,
                "date": new Date(2017, 10, 12),
                "location": "Petrova 3"
            },
        ];
        this.bloodDonations = data;
        console.log(this.bloodDonations);
        this.dataSource.data = this.bloodDonations;
        // this.bloodDonorService.getAllBloodSupply().toPromise().then((response) => {
        //   this.bloodSupplies = (<HttpResponse<BloodSupply[]>>response).body;
        //   this.dataSource.data = this.bloodSupplies;
        // }).catch((err) => {
        //   console.error(err);
        // })
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSort"])
    ], DonorProfileComponent.prototype, "sort", void 0);
    DonorProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-donor-profile',
            template: __webpack_require__(/*! ./donor-profile.component.html */ "./src/app/component/donor-profile/donor-profile.component.html"),
            styles: [__webpack_require__(/*! ./donor-profile.component.css */ "./src/app/component/donor-profile/donor-profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"], angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_2__["ToasterService"]])
    ], DonorProfileComponent);
    return DonorProfileComponent;
}());



/***/ }),

/***/ "./src/app/component/home/home.component.css":
/*!***************************************************!*\
  !*** ./src/app/component/home/home.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/component/home/home.component.html":
/*!****************************************************!*\
  !*** ./src/app/component/home/home.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div style=\"display: block; margin-left: auto; margin-right: auto; width: 100%; margin-top: 5em;\">\n    <img src=\"/assets/blood-drop.svg\" style=\"width:300px; margin-left: calc(50% - 150px); margin-right: auto\">\n  </div>\n  <div style=\"display: block; margin-top: 0.75em\">\n    <h3 style=\"font-family: 'Lato'; font-size: 28pt; display: block; vertical-align: middle; text-align: center; padding-top: 2.0em; color: #323232\">Krv za život</h3>\n    <span style=\"font-family: 'Lato'; font-size: 18pt; display: block; text-align: center; margin-top: 0.5em; margin-bottom: 2.5em; font-style: italic\"> Prijavite se u najveću online bazu dobrovoljnih darivatelja krvi.</span>\n    <a style=\"font-family: 'Lato'; display: block; width: 300px; background-color: #D80027; border-radius: 50px; color: white; font-weight: bold; font-size: 14pt; padding: 0.5em; text-align: center; margin-left: auto; margin-right: auto\" mat-button href=\"http://localhost:8081/auth/login\">PRIJAVA/REGISTRACIJA</a>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/home/home.component.ts":
/*!**************************************************!*\
  !*** ./src/app/component/home/home.component.ts ***!
  \**************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/component/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/component/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/component/notification/notification/notification.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/component/notification/notification/notification.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".notification-container {\n  margin-top: 2em;\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px;\n  overflow: auto;\n}\n\n.unread-notification {\n  margin: 0.25em;\n  box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.15);\n  background-color: rgba(255, 0, 0, 0.21);\n}\n\n.read-notification {\n  margin: 0.25em;\n  box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.15);\n  background-color: whitesmoke;\n}\n"

/***/ }),

/***/ "./src/app/component/notification/notification/notification.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/component/notification/notification/notification.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"notification-container\">\n  <div class=\"unread-notification\" *ngFor=\"let notification of notifications\">\n    <h3>\n      <mat-icon style=\"vertical-align: middle\">notifications</mat-icon>&nbsp;{{notification.title}}\n    </h3>\n    <div style=\"padding: 1.5em;\">\n      {{notification.content}}\n    </div>\n  </div>\n  <div class=\"read-notification\" *ngFor=\"let notification of notifications\">\n    <h3>\n      <mat-icon>notifications_none</mat-icon>{{notification.title}}\n    </h3>\n    <div style=\"padding: 1.5em;\">\n      {{notification.content}}\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/notification/notification/notification.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/component/notification/notification/notification.component.ts ***!
  \*******************************************************************************/
/*! exports provided: NotificationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationComponent", function() { return NotificationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_notification_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../service/notification-service.service */ "./src/app/service/notification-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationComponent = /** @class */ (function () {
    function NotificationComponent(notificationService) {
        this.notificationService = notificationService;
    }
    NotificationComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    NotificationComponent.prototype.loadData = function () {
        var _this = this;
        this.notificationService.getAllNotifications()
            .then(function (response) {
            _this.notifications = response;
        }).catch(function (err) {
            console.error(err);
        });
    };
    NotificationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notification',
            template: __webpack_require__(/*! ./notification.component.html */ "./src/app/component/notification/notification/notification.component.html"),
            styles: [__webpack_require__(/*! ./notification.component.css */ "./src/app/component/notification/notification/notification.component.css")]
        }),
        __metadata("design:paramtypes", [_service_notification_service_service__WEBPACK_IMPORTED_MODULE_1__["NotificationServiceService"]])
    ], NotificationComponent);
    return NotificationComponent;
}());



/***/ }),

/***/ "./src/app/component/poll/poll/poll-new.dialog.html":
/*!**********************************************************!*\
  !*** ./src/app/component/poll/poll/poll-new.dialog.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div mat-dialog-title>\n    <h1 mat-dialog-title>{{data.title}}</h1>\n  </div>\n  <div mat-dialog-content>\n    <form>\n      <mat-form-field class=\"dialog-input\" appearance=\"standard\">\n        <mat-label>Pitanje</mat-label>\n        <input matInput [(ngModel)]=\"newPoll.name\" placeholder=\"Unesite pitanje...\" name=\"name\">\n      </mat-form-field>\n\n      <mat-form-field class=\"dialog-input\" appearance=\"standard\">\n        <mat-label>Rizično</mat-label>\n        <mat-select [(ngModel)]=\"newPoll.isCritical\" placeholder=\"Odaberite rizičnost\" name=\"isCritical\">\n          <mat-option [value]=\"null\"></mat-option>\n          <mat-option [value]=\"true\">Rizično</mat-option>\n          <mat-option [value]=\"false\">Nije rizično</mat-option>\n        </mat-select>\n      </mat-form-field>\n    </form>\n  </div>\n  <div mat-dialog-actions class=\"dialog-footer\">\n    <button mat-button (click)=\"closeDialog()\">Odustani</button>\n    <button mat-button [mat-dialog-close]=\"newPoll\" cdkFocusInitial>Spremi</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/poll/poll/poll-new.dialog.ts":
/*!********************************************************!*\
  !*** ./src/app/component/poll/poll/poll-new.dialog.ts ***!
  \********************************************************/
/*! exports provided: PollNewDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PollNewDialog", function() { return PollNewDialog; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var PollNewDialog = /** @class */ (function () {
    function PollNewDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.newPoll = Object.assign({}, data.category);
        console.log(data);
    }
    PollNewDialog.prototype.closeDialog = function () {
        this.dialogRef.close(null);
    };
    PollNewDialog = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "dialog-poll-new",
            template: __webpack_require__(/*! ./poll-new.dialog.html */ "./src/app/component/poll/poll/poll-new.dialog.html"),
            styles: [__webpack_require__(/*! ./poll.component.css */ "./src/app/component/poll/poll/poll.component.css")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], PollNewDialog);
    return PollNewDialog;
}());



/***/ }),

/***/ "./src/app/component/poll/poll/poll.component.css":
/*!********************************************************!*\
  !*** ./src/app/component/poll/poll/poll.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-container {\n  margin-top: 2em;\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px;\n  overflow: auto;\n}\n\n.dialog-input {\n  width: 100%;\n}\n\n.dialog-footer {\n  margin: 1.25em 0 0.5em auto;\n  float: right\n}\n\n.example-header {\n  background-color: white;\n  min-height: 64px;\n  padding: 8px 24px 0;\n}\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%;\n}\n"

/***/ }),

/***/ "./src/app/component/poll/poll/poll.component.html":
/*!*********************************************************!*\
  !*** ./src/app/component/poll/poll/poll.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-container mat-elevation-z1\">\n  <mat-toolbar class=\"mat-elevation-z1\">Pitanja&nbsp;&nbsp;<button mat-icon-button\n                                                                   (click)=\"openNewDialog()\">\n    <mat-icon>add</mat-icon>\n  </button>\n  </mat-toolbar>\n\n  <table mat-table matSort #pollTable [dataSource]=\"dataSource\">\n    <ng-container matColumnDef=\"name\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Pitanje</th>\n      <td mat-cell *matCellDef=\"let element\"> {{element.name}}</td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"isCritical\">\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>Rizično</th>\n      <td mat-cell *matCellDef=\"let element\"> {{ element.isCritical ? 'Rizično' : 'Nije rizično' }}</td>\n    </ng-container>\n\n    <!-- Symbol Column -->\n    <ng-container matColumnDef=\"actions\">\n      <th mat-header-cell *matHeaderCellDef> Akcije</th>\n      <td mat-cell *matCellDef=\"let element\" style=\"flex-basis: 85px !important;\">\n        <button mat-icon-button (click)=\"openEditDialog(element)\">\n          <mat-icon>edit</mat-icon>\n        </button>\n        <button mat-icon-button (click)=\"deletePoll(element)\">\n          <mat-icon>delete</mat-icon>\n        </button>\n      </td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\n  </table>\n</div>\n\n"

/***/ }),

/***/ "./src/app/component/poll/poll/poll.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/component/poll/poll/poll.component.ts ***!
  \*******************************************************/
/*! exports provided: PollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PollComponent", function() { return PollComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_poll_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../service/poll-service.service */ "./src/app/service/poll-service.service.ts");
/* harmony import */ var _domain_BloodSupply__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../domain/BloodSupply */ "./src/app/domain/BloodSupply.ts");
/* harmony import */ var _shared_confirm_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/confirm.dialog */ "./src/app/component/shared/confirm.dialog.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular5-toaster/dist */ "./node_modules/angular5-toaster/dist/angular5-toaster.js");
/* harmony import */ var _poll_new_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./poll-new.dialog */ "./src/app/component/poll/poll/poll-new.dialog.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PollComponent = /** @class */ (function () {
    function PollComponent(pollService, dialog, toasterService) {
        this.pollService = pollService;
        this.dialog = dialog;
        this.toasterService = toasterService;
        this.POLL_CREATE = "Stvori novo pitanje";
        this.POLL_EDIT = "Uredi pitanje";
        this.displayedColumns = ['name', 'isCritical'];
    }
    PollComponent.prototype.ngOnInit = function () {
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"](this.polls);
        this.loadData();
    };
    PollComponent.prototype.loadData = function () {
        var _this = this;
        this.pollService.getAllPolls().toPromise()
            .then(function (response) {
            _this.polls = response.body;
            _this.dataSource.data = _this.polls;
        }).catch(function (err) {
            console.error(err);
        });
    };
    PollComponent.prototype.refreshPolls = function () {
        var _this = this;
        this.pollService.getAllPolls().toPromise().then(function (response) {
            _this.polls = response.body;
            _this.dataSource.data = _this.polls;
        }).catch(function (err) {
            console.error(err);
        });
    };
    PollComponent.prototype.openNewDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_poll_new_dialog__WEBPACK_IMPORTED_MODULE_6__["PollNewDialog"], {
            width: "500px",
            data: {
                title: this.POLL_CREATE,
                parentCategories: this.polls,
                category: new _domain_BloodSupply__WEBPACK_IMPORTED_MODULE_2__["BloodSupply"]()
            },
            disableClose: true
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result == null || Object.keys(result).length == 0)
                return;
            _this.pollService.savePoll(result).toPromise().then(function (response) {
                _this.toasterService.pop("success", "Uspješno je", "dodano novo pitanje: " + result.name);
                _this.refreshPolls();
            });
        });
    };
    PollComponent.prototype.openEditDialog = function (poll) {
        var _this = this;
        var dialogRef = this.dialog.open(_poll_new_dialog__WEBPACK_IMPORTED_MODULE_6__["PollNewDialog"], {
            width: "500px",
            data: {
                title: this.POLL_EDIT,
                poll: poll
            },
            disableClose: true
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result == null || Object.keys(result).length == 0)
                return;
            _this.pollService.editPoll(result).toPromise().then(function (response) {
                _this.toasterService.pop("success", "Uspješno je", "ure\u0111eno pitanje: " + result.name);
                _this.refreshPolls();
            });
        });
    };
    PollComponent.prototype.deletePoll = function (poll) {
        var _this = this;
        var dialogRef = this.dialog.open(_shared_confirm_dialog__WEBPACK_IMPORTED_MODULE_3__["ConfirmDialog"], {
            width: "500px",
            data: {
                title: "Jeste li sigurni?",
                message: "Pitanje će biti trajno uklonjeno i nece biti moguće poništiti rezultat ove akcije."
            },
            disableClose: true
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result == null)
                return;
            _this.pollService.editPoll(result).toPromise().then(function (response) {
                _this.pollService.deletePoll(poll).toPromise().then(function (result) {
                    _this.toasterService.pop("success", "Uspješno je", "obrisana kategorija: " + poll.name);
                    _this.refreshPolls();
                });
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSort"])
    ], PollComponent.prototype, "sort", void 0);
    PollComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-poll',
            template: __webpack_require__(/*! ./poll.component.html */ "./src/app/component/poll/poll/poll.component.html"),
            styles: [__webpack_require__(/*! ./poll.component.css */ "./src/app/component/poll/poll/poll.component.css")]
        }),
        __metadata("design:paramtypes", [_service_poll_service_service__WEBPACK_IMPORTED_MODULE_1__["PollServiceService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"], angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_5__["ToasterService"]])
    ], PollComponent);
    return PollComponent;
}());



/***/ }),

/***/ "./src/app/component/qrcode/qrcode.component.css":
/*!*******************************************************!*\
  !*** ./src/app/component/qrcode/qrcode.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".qr-container {\n  margin-top: 2em;\n  display: flex;\n  flex-direction: column;\n  max-height: 500px;\n  min-width: 300px;\n  overflow: auto;\n  background-color: whitesmoke;\n}\n"

/***/ }),

/***/ "./src/app/component/qrcode/qrcode.component.html":
/*!********************************************************!*\
  !*** ./src/app/component/qrcode/qrcode.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"qr-container\">\n  <mat-toolbar class=\"mat-elevation-z0\">Potvrdi donaciju</mat-toolbar>\n  <qrcode style=\"margin-left: auto; margin-right: auto; margin-top: 2em\" [qrdata]=\"{'userId':'1', 'date': '2018-03-13'}\" [size]=\"256\" [level]=\"'M'\"></qrcode>\n  <div style=\"text-align: center; margin-top: 0.5em\">\n    <h4>Ivan Horvat</h4>\n    <h5>13.03.2018</h5>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/qrcode/qrcode.component.ts":
/*!******************************************************!*\
  !*** ./src/app/component/qrcode/qrcode.component.ts ***!
  \******************************************************/
/*! exports provided: QrcodeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrcodeComponent", function() { return QrcodeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var QrcodeComponent = /** @class */ (function () {
    function QrcodeComponent() {
    }
    QrcodeComponent.prototype.ngOnInit = function () {
    };
    QrcodeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-qrcode',
            template: __webpack_require__(/*! ./qrcode.component.html */ "./src/app/component/qrcode/qrcode.component.html"),
            styles: [__webpack_require__(/*! ./qrcode.component.css */ "./src/app/component/qrcode/qrcode.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], QrcodeComponent);
    return QrcodeComponent;
}());



/***/ }),

/***/ "./src/app/component/shared/confirm.dialog.html":
/*!******************************************************!*\
  !*** ./src/app/component/shared/confirm.dialog.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div mat-dialog-title>\n<h1 mat-dialog-title>{{data.title}}</h1>\n</div>\n<div mat-dialog-content>\n <h5>{{data.message}}</h5>\n</div>\n<div mat-dialog-actions class=\"dialog-footer\">\n<button mat-button (click)=\"closeDialog()\">Odustani</button>\n  <button mat-button [mat-dialog-close]=\"true\" cdkFocusInitial>Potvrdi</button>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/component/shared/confirm.dialog.ts":
/*!****************************************************!*\
  !*** ./src/app/component/shared/confirm.dialog.ts ***!
  \****************************************************/
/*! exports provided: ConfirmDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmDialog", function() { return ConfirmDialog; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ConfirmDialog = /** @class */ (function () {
    function ConfirmDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ConfirmDialog.prototype.closeDialog = function () {
        this.dialogRef.close(null);
    };
    ConfirmDialog = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "dialog-confirm",
            template: __webpack_require__(/*! ./confirm.dialog.html */ "./src/app/component/shared/confirm.dialog.html"),
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], ConfirmDialog);
    return ConfirmDialog;
}());



/***/ }),

/***/ "./src/app/domain/BloodSupply.ts":
/*!***************************************!*\
  !*** ./src/app/domain/BloodSupply.ts ***!
  \***************************************/
/*! exports provided: BloodSupply */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloodSupply", function() { return BloodSupply; });
var BloodSupply = /** @class */ (function () {
    function BloodSupply() {
    }
    return BloodSupply;
}());



/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"]
            ],
            exports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/service/additional-data-service.service.ts":
/*!************************************************************!*\
  !*** ./src/app/service/additional-data-service.service.ts ***!
  \************************************************************/
/*! exports provided: AdditionalDataServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdditionalDataServiceService", function() { return AdditionalDataServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./loader.service */ "./src/app/service/loader.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdditionalDataServiceService = /** @class */ (function () {
    function AdditionalDataServiceService(http, loaderService) {
        this.http = http;
        this.loaderService = loaderService;
        this.API_ENDPOINT = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api.prefix.api + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api.endpoint.api.additionalData;
    }
    AdditionalDataServiceService.prototype.getAdditionalDataForDonor = function (userId) {
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]("GET", this.API_ENDPOINT + "/" + userId, {
            reportProgress: true,
            observe: 'events'
        });
        return this.http.request(req);
    };
    AdditionalDataServiceService.prototype.getEventMessage = function (event, payload) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Sent:
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress:
                // Compute and show the % done:
                return Math.round(100 * event.loaded / event.total);
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].DownloadProgress:
                // Compute and show the % done: --> cannot calculate this because total size isn't known
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response:
                this.loaderService.setState(_loader_service__WEBPACK_IMPORTED_MODULE_2__["State"].LOADED);
                return event;
            default:
                return event;
        }
    };
    AdditionalDataServiceService.prototype.showProgress = function (message) {
        if (typeof message === 'number') {
            // notify progress bar... maybe...
        }
    };
    AdditionalDataServiceService.prototype.handleError = function (event) {
        return undefined;
    };
    AdditionalDataServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]])
    ], AdditionalDataServiceService);
    return AdditionalDataServiceService;
}());



/***/ }),

/***/ "./src/app/service/app-router.module.ts":
/*!**********************************************!*\
  !*** ./src/app/service/app-router.module.ts ***!
  \**********************************************/
/*! exports provided: AppRouterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRouterModule", function() { return AppRouterModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _component_home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/home/home.component */ "./src/app/component/home/home.component.ts");
/* harmony import */ var _component_bloodSupply_edit_bloodSupply_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../component/bloodSupply/edit-bloodSupply.component */ "./src/app/component/bloodSupply/edit-bloodSupply.component.ts");
/* harmony import */ var _component_bloodDonor_blood_donor_table_blood_donor_table_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../component/bloodDonor/blood-donor-table/blood-donor-table.component */ "./src/app/component/bloodDonor/blood-donor-table/blood-donor-table.component.ts");
/* harmony import */ var _component_donor_profile_donor_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../component/donor-profile/donor-profile.component */ "./src/app/component/donor-profile/donor-profile.component.ts");
/* harmony import */ var _component_notification_notification_notification_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../component/notification/notification/notification.component */ "./src/app/component/notification/notification/notification.component.ts");
/* harmony import */ var _component_poll_poll_poll_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../component/poll/poll/poll.component */ "./src/app/component/poll/poll/poll.component.ts");
/* harmony import */ var _component_additionalData_additional_data_additional_data_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../component/additionalData/additional-data/additional-data.component */ "./src/app/component/additionalData/additional-data/additional-data.component.ts");
/* harmony import */ var _component_qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../component/qrcode/qrcode.component */ "./src/app/component/qrcode/qrcode.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    { path: '', component: _component_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: 'blood-supplies', component: _component_bloodSupply_edit_bloodSupply_component__WEBPACK_IMPORTED_MODULE_3__["BloodSupplyComponent"] },
    { path: 'donor-profile', component: _component_donor_profile_donor_profile_component__WEBPACK_IMPORTED_MODULE_5__["DonorProfileComponent"] },
    { path: 'users', component: _component_bloodDonor_blood_donor_table_blood_donor_table_component__WEBPACK_IMPORTED_MODULE_4__["BloodDonorTableComponent"] },
    { path: 'notifications', component: _component_notification_notification_notification_component__WEBPACK_IMPORTED_MODULE_6__["NotificationComponent"] },
    { path: 'polls', component: _component_poll_poll_poll_component__WEBPACK_IMPORTED_MODULE_7__["PollComponent"] },
    { path: 'additional-data', component: _component_additionalData_additional_data_additional_data_component__WEBPACK_IMPORTED_MODULE_8__["AdditionalDataComponent"] },
    { path: 'qr-generator', component: _component_qrcode_qrcode_component__WEBPACK_IMPORTED_MODULE_9__["QrcodeComponent"] },
];
var AppRouterModule = /** @class */ (function () {
    function AppRouterModule() {
    }
    AppRouterModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRouterModule);
    return AppRouterModule;
}());



/***/ }),

/***/ "./src/app/service/auth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Role = /** @class */ (function () {
    function Role() {
    }
    return Role;
}());
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.user = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](new User());
    }
    AuthService.prototype.getLoggedInUser = function () {
        var _this = this;
        console.log("Constructor");
        this.http.get("http://localhost:8081/auth/users/roles/logged").toPromise().then(function (p) {
            console.log(p);
            _this.user.next(p);
        });
    };
    AuthService.prototype.ngOnInit = function () {
        this.getLoggedInUser();
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/service/bloodDonor.service.ts":
/*!***********************************************!*\
  !*** ./src/app/service/bloodDonor.service.ts ***!
  \***********************************************/
/*! exports provided: BloodDonorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloodDonorService", function() { return BloodDonorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loader.service */ "./src/app/service/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BloodDonorService = /** @class */ (function () {
    function BloodDonorService(http, loaderService) {
        this.http = http;
        this.loaderService = loaderService;
        this.API_ENDPOINT = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].api.prefix.api + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].api.endpoint.api.bloodDonor;
    }
    BloodDonorService.prototype.getAllDonors = function () {
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpRequest"]("GET", this.API_ENDPOINT, {
            reportProgress: true,
            observe: 'events'
        });
        return this.http.request(req);
    };
    BloodDonorService.prototype.getEventMessage = function (event, payload) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Sent:
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress:
                // Compute and show the % done:
                return Math.round(100 * event.loaded / event.total);
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].DownloadProgress:
                // Compute and show the % done: --> cannot calculate this because total size isn't known
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Response:
                this.loaderService.setState(_loader_service__WEBPACK_IMPORTED_MODULE_3__["State"].LOADED);
                return event;
            default:
                return event;
        }
    };
    BloodDonorService.prototype.showProgress = function (message) {
        if (typeof message === 'number') {
            // notify progress bar... maybe...
        }
    };
    BloodDonorService.prototype.handleError = function (event) {
        return undefined;
    };
    BloodDonorService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"]])
    ], BloodDonorService);
    return BloodDonorService;
}());



/***/ }),

/***/ "./src/app/service/bloodSupply.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/bloodSupply.service.ts ***!
  \************************************************/
/*! exports provided: BloodSupplyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloodSupplyService", function() { return BloodSupplyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./loader.service */ "./src/app/service/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import {Observable} from "rxjs/internal/Observable";


var BloodSupplyService = /** @class */ (function () {
    function BloodSupplyService(http, loaderService) {
        this.http = http;
        this.loaderService = loaderService;
        this.API_ENDPOINT = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.prefix.api + _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api.endpoint.api.bloodSupply;
    }
    BloodSupplyService.prototype.getAllBloodSupply = function () {
        var _this = this;
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]("GET", this.API_ENDPOINT, {
            reportProgress: true,
            observe: 'events'
        });
        return this.http.request(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (event) { return _this.getEventMessage(event, null); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (message) { return _this.showProgress(message); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(100), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["last"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError(event)));
    };
    BloodSupplyService.prototype.getBloodSupply = function (id) {
        return this.http.get(this.API_ENDPOINT + "/" + id, {
            reportProgress: true,
            headers: {}
        });
    };
    BloodSupplyService.prototype.saveBloodSupply = function (bloodSupply) {
        return this.http.post("" + this.API_ENDPOINT, bloodSupply, {
            reportProgress: true,
            headers: {}
        });
    };
    BloodSupplyService.prototype.editBloodSupply = function (bloodSupply) {
        return this.http.put("" + this.API_ENDPOINT, bloodSupply, {
            reportProgress: true,
            headers: {}
        });
    };
    BloodSupplyService.prototype.deleteCategory = function (bloodSupply) {
        return this.http.delete(this.API_ENDPOINT + "/" + bloodSupply.id, {
            reportProgress: true,
            headers: {}
        });
    };
    BloodSupplyService.prototype.bloodSupplyRequest = function (request) {
        return this.http.post(this.API_ENDPOINT + "/supply", request, {
            reportProgress: true,
            headers: {}
        });
    };
    BloodSupplyService.prototype.getEventMessage = function (event, payload) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Sent:
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress:
                // Compute and show the % done:
                return Math.round(100 * event.loaded / event.total);
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].DownloadProgress:
                // Compute and show the % done: --> cannot calculate this because total size isn't known
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response:
                this.loaderService.setState(_loader_service__WEBPACK_IMPORTED_MODULE_4__["State"].LOADED);
                return event;
            default:
                return event;
        }
    };
    BloodSupplyService.prototype.showProgress = function (message) {
        if (typeof message === 'number') {
            // notify progress bar... maybe...
        }
    };
    BloodSupplyService.prototype.handleError = function (event) {
        return undefined;
    };
    BloodSupplyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _loader_service__WEBPACK_IMPORTED_MODULE_4__["LoaderService"]])
    ], BloodSupplyService);
    return BloodSupplyService;
}());



/***/ }),

/***/ "./src/app/service/global-error-handler.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/service/global-error-handler.service.ts ***!
  \*********************************************************/
/*! exports provided: GlobalErrorHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalErrorHandlerService", function() { return GlobalErrorHandlerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loader.service */ "./src/app/service/loader.service.ts");
/* harmony import */ var angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular5-toaster/dist */ "./node_modules/angular5-toaster/dist/angular5-toaster.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GlobalErrorHandlerService = /** @class */ (function () {
    function GlobalErrorHandlerService(loaderService, toasterService) {
        this.loaderService = loaderService;
        this.toasterService = toasterService;
    }
    GlobalErrorHandlerService.prototype.handleError = function (error) {
        this.loaderService.setState(_loader_service__WEBPACK_IMPORTED_MODULE_1__["State"].LOADED);
        this.toasterService.pop("error", "Dogodila se greška", error.message);
        throw error;
    };
    GlobalErrorHandlerService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_loader_service__WEBPACK_IMPORTED_MODULE_1__["LoaderService"], angular5_toaster_dist__WEBPACK_IMPORTED_MODULE_2__["ToasterService"]])
    ], GlobalErrorHandlerService);
    return GlobalErrorHandlerService;
}());



/***/ }),

/***/ "./src/app/service/loader.service.ts":
/*!*******************************************!*\
  !*** ./src/app/service/loader.service.ts ***!
  \*******************************************/
/*! exports provided: State, LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "State", function() { return State; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var State;
(function (State) {
    State[State["LOADING"] = 0] = "LOADING";
    State[State["LOADED"] = 1] = "LOADED";
})(State || (State = {}));
var LoaderService = /** @class */ (function () {
    function LoaderService() {
        this.isLoading = false;
        this.subject = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](false);
    }
    LoaderService.prototype.setState = function (state) {
        this.isLoading = state == State.LOADING;
        this.subject.next(this.isLoading);
    };
    LoaderService.prototype.getLoadingState = function () {
        return this.subject.asObservable().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["delay"])(10));
    };
    LoaderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "./src/app/service/notification-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/service/notification-service.service.ts ***!
  \*********************************************************/
/*! exports provided: NotificationServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationServiceService", function() { return NotificationServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./loader.service */ "./src/app/service/loader.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationServiceService = /** @class */ (function () {
    function NotificationServiceService(http, loaderService) {
        this.http = http;
        this.loaderService = loaderService;
        this.INFO_API_ENDPOINT = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api.prefix.api + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api.endpoint.api.infoNotifications;
        this.DONATION_REQUEST_API_ENDPOINT = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api.prefix.api + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].api.endpoint.api.donationRequestNotifications;
    }
    NotificationServiceService.prototype.getAllNotifications = function () {
        var _this = this;
        var reqInfoNotification = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]("GET", this.INFO_API_ENDPOINT, {
            reportProgress: true,
            observe: 'events'
        });
        var reqDonationRequest = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]("GET", this.DONATION_REQUEST_API_ENDPOINT, {
            reportProgress: true,
            observe: 'events'
        });
        var notificationList = [];
        return this.http.request(reqInfoNotification).toPromise().then(function (data) {
            notificationList = notificationList.concat(data.body);
            return _this.http.request(reqDonationRequest).toPromise().then(function (data) {
                return notificationList.concat(data.body);
            });
        });
    };
    NotificationServiceService.prototype.getEventMessage = function (event, payload) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Sent:
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress:
                // Compute and show the % done:
                return Math.round(100 * event.loaded / event.total);
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].DownloadProgress:
                // Compute and show the % done: --> cannot calculate this because total size isn't known
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response:
                this.loaderService.setState(_loader_service__WEBPACK_IMPORTED_MODULE_2__["State"].LOADED);
                return event;
            default:
                return event;
        }
    };
    NotificationServiceService.prototype.showProgress = function (message) {
        if (typeof message === 'number') {
            // notify progress bar... maybe...
        }
    };
    NotificationServiceService.prototype.handleError = function (event) {
        return undefined;
    };
    NotificationServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]])
    ], NotificationServiceService);
    return NotificationServiceService;
}());



/***/ }),

/***/ "./src/app/service/poll-service.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/poll-service.service.ts ***!
  \*************************************************/
/*! exports provided: PollServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PollServiceService", function() { return PollServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loader.service */ "./src/app/service/loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PollServiceService = /** @class */ (function () {
    function PollServiceService(http, loaderService) {
        this.http = http;
        this.loaderService = loaderService;
        this.API_ENDPOINT = "" + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].api.prefix.api + _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].api.endpoint.api.polls;
    }
    PollServiceService.prototype.getAllPolls = function () {
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpRequest"]("GET", this.API_ENDPOINT, {
            reportProgress: true,
            observe: 'events'
        });
        return this.http.request(req);
    };
    PollServiceService.prototype.savePoll = function (poll) {
        return this.http.post("" + this.API_ENDPOINT, poll, {
            reportProgress: true,
            headers: {}
        });
    };
    PollServiceService.prototype.editPoll = function (poll) {
        return this.http.put("" + this.API_ENDPOINT, poll, {
            reportProgress: true,
            headers: {}
        });
    };
    PollServiceService.prototype.deletePoll = function (poll) {
        return this.http.delete(this.API_ENDPOINT + "/" + poll.id, {
            reportProgress: true,
            headers: {}
        });
    };
    PollServiceService.prototype.getEventMessage = function (event, payload) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Sent:
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress:
                // Compute and show the % done:
                return Math.round(100 * event.loaded / event.total);
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].DownloadProgress:
                // Compute and show the % done: --> cannot calculate this because total size isn't known
                return event;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Response:
                this.loaderService.setState(_loader_service__WEBPACK_IMPORTED_MODULE_3__["State"].LOADED);
                return event;
            default:
                return event;
        }
    };
    PollServiceService.prototype.showProgress = function (message) {
        if (typeof message === 'number') {
            // notify progress bar... maybe...
        }
    };
    PollServiceService.prototype.handleError = function (event) {
        return undefined;
    };
    PollServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _loader_service__WEBPACK_IMPORTED_MODULE_3__["LoaderService"]])
    ], PollServiceService);
    return PollServiceService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    api: {
        prefix: {
            api: "/api"
        },
        endpoint: {
            api: {
                bloodSupply: "/blood-supplies",
                bloodDonor: "/blood-donors",
                infoNotifications: "/info-notification",
                donationRequestNotifications: "/donation-request-notification",
                polls: "/poll-data",
                additionalData: "/additional-data"
            },
        }
    }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/mbartolac/IdeaProjects/fer/hackathon-combis-2018/angular-app/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map