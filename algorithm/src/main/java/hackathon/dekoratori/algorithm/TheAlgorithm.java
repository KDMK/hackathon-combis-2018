package hackathon.dekoratori.algorithm;

import hackathon.dekoratori.algorithm.constant.Constants;
import hackathon.dekoratori.algorithm.genetic.BloodType;
import hackathon.dekoratori.algorithm.genetic.Donor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class TheAlgorithm {

	private Map<BloodType, Double> factors = new HashMap<>();
	private int[] currentBloodLevels;

	@Autowired
	private Constants constants;

	private List<Donor> donors;

	private Map<BloodType, List<Donor>> bloodDonors;

	public void init(List<Donor> donors, int[] currentBloodLevels) {
		this.donors = donors;
		removeCalled(donors);
		initializeBloodDonors();
		this.currentBloodLevels = currentBloodLevels;
		double ABP_FACTOR = 2.2;
		factors.put(BloodType.ABP, ABP_FACTOR);
		double ABM_FACTOR = 2.2;
		factors.put(BloodType.ABM, ABM_FACTOR);
		double OM_FACTOR = 4;
		factors.put(BloodType.OM, OM_FACTOR);
		double OP_FACTOR = 3;
		factors.put(BloodType.OP, OP_FACTOR);
		double AM_FACTOR = 3;
		factors.put(BloodType.AM, AM_FACTOR);
		double AP_FACTOR = 2.7;
		factors.put(BloodType.AP, AP_FACTOR);
		double BM_FACTOR = 2.4;
		factors.put(BloodType.BM, BM_FACTOR);
		double BP_FACTOR = 2.5;
		factors.put(BloodType.BP, BP_FACTOR);
	}

	private void removeCalled(List<Donor> donors) {
		List<Donor> tempDonors = new ArrayList<>(donors);
		for (Donor donor : tempDonors) {
			if (donor.getSex().equals("M") && donor.getLast_donation() + 7 * 0 - 90 < 0 || (donor.getSex().equals("Z") && donor.getLast_donation() + 7 * 0 - 120 < 0)) {
				donors.remove(donor);
			}
		}
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get("called.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (lines.isEmpty()) {
			return;
		}

		String[] ids = lines.get(0).split(",");
		for (String id : ids) {
			for (Donor donor : tempDonors) {
				if (donor.getId().equals(id)) {
					donors.remove(donor);
				}
			}
		}
	}

	private void initializeBloodDonors() {
		bloodDonors = donors.stream().collect(Collectors.groupingBy(d -> BloodType.getByString(d.getBlood_group())));
	}

	public List<Donor> getBloodDonors() {
		List<Donor> toCallDonors = new ArrayList<>();
		BloodType[] values = BloodType.values();
		for (int i = 0; i < values.length; i++) {
			BloodType type = values[i];
			int optimalBloodLevel = constants.getLimits().get(type).getOptimum();
			int consumption = constants.getLimits().get(type).getConsumption();
			int numberOfDonors = (int) ((optimalBloodLevel - currentBloodLevels[i] + consumption) * factors.get(type) * 1);
			if (numberOfDonors < 0) {
				numberOfDonors = 0;
			}

			List<Donor> donors = bloodDonors.get(type)
					.stream()
					.sorted(Comparator.comparingDouble(this::callProbability).reversed()
					).collect(Collectors.toList());

			toCallDonors.addAll(donors.stream().limit(numberOfDonors).collect(Collectors.toList()));
		}

		return toCallDonors;
	}

	private double callProbability(Donor donor) {
		double sexProbability;
		if (donor.getSex().equals("M")) {
			sexProbability = donor.getFrequency() / 4.0;
		} else {
			sexProbability = donor.getFrequency() / 3.0;
		}
		double distanceProbability = 1 - donor.getDistance() / 30.0;

		return distanceProbability * sexProbability;
	}
}