package hackathon.dekoratori.algorithm.constant;

import hackathon.dekoratori.algorithm.genetic.BloodType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Data
public class Constants {

	private Map<BloodType, Limits> limits = new HashMap() {{
		put(BloodType.OM, new Limits(38, 78, 58, 35));
		put(BloodType.OP, new Limits(115, 240, 177, 105));
		put(BloodType.AM, new Limits(46, 96, 71, 42));
		put(BloodType.AP, new Limits(100, 210, 155, 91));
		put(BloodType.BM, new Limits(38, 82, 60, 35));
		put(BloodType.BP, new Limits(23, 50, 36, 21));
		put(BloodType.ABM, new Limits(8, 18, 13, 7));
		put(BloodType.ABP, new Limits(16, 36, 26, 14));
	}};

	private double frequencyWeight = 0.5;

	public class Limits {
		private int min;
		private int max;
		private int optimum;
		private int consumption;

		public Limits(int min, int max, int optimum, int consumption) {
			this.min = min;
			this.max = max;
			this.optimum = optimum;
			this.consumption = consumption;
		}

		public int getMin() {
			return min;
		}

		public void setMin(int min) {
			this.min = min;
		}

		public int getMax() {
			return max;
		}

		public void setMax(int max) {
			this.max = max;
		}

		public int getOptimum() {
			return optimum;
		}

		public void setOptimum(int optimum) {
			this.optimum = optimum;
		}

		public int getConsumption() {
			return consumption;
		}

		public void setConsumption(int consumption) {
			this.consumption = consumption;
		}
	}
}
