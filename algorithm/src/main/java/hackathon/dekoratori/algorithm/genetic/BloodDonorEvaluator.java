package hackathon.dekoratori.algorithm.genetic;

import hackathon.dekoratori.algorithm.constant.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BloodDonorEvaluator implements Evaluator {

	private static final int NUM_OF_BLOOD_TYPES = 8;

	private int[] currentBloodLevels;

	private Random random = new Random();

	@Autowired
	private Constants constants;

	private List<Donor> donors;

	private Map<BloodType, List<Donor>> bloodDonors;

	public void init(List<Donor> donors, int[] currentBloodLevels) {
		this.donors = donors;
		initializeBloodDonors();
		this.currentBloodLevels = currentBloodLevels;
	}

	private void initializeBloodDonors() {
//		double min = Double.MAX_VALUE;
//		double max = -Double.MAX_VALUE;
//		for (Donor donor : donors) {
//			if (donor.getDistance() < min) {
//				min = donor.getDistance();
//			} else if (donor.getDistance() > max) {
//				max = donor.getDistance();
//			}
//		}
//
//		for (Donor donor : donors) {
//			donor.setDistance(100 - ((donor.getDistance() - min) / (max - min) * 100));
//			if (donor.getSex().equals("M")) {
//				donor.setFrequency(donor.getFrequency() * 25);
//			} else {
//				donor.setFrequency(donor.getFrequency() * 33);
//			}
//		}

		bloodDonors = donors.stream().collect(Collectors.groupingBy(d -> BloodType.getByString(d.getBlood_group())));
	}

	@Override
	public double evaluate(double[] solution) {
		double totalLoss = 0;

		double weight = solution[NUM_OF_BLOOD_TYPES];

		BloodType[] values = BloodType.values();
		for (int i = 0; i < values.length; i++) {
			BloodType type = values[i];
			int bloodLevel = currentBloodLevels[i];
			int optimalBloodLevel = constants.getLimits().get(type).getOptimum();
			int minBloodLevel = constants.getLimits().get(type).getMin();
			int maxBloodLevel = constants.getLimits().get(type).getMax();
			int consumption = constants.getLimits().get(type).getConsumption();
			int numberOfDonors = (int) ((optimalBloodLevel - currentBloodLevels[i] + consumption) * solution[i]);
			if (numberOfDonors < 1) {
				continue;
			}

			List<Donor> donors = bloodDonors.get(type).stream()
					.sorted(Comparator.comparingDouble((Donor donor) -> donor.getDistance() * weight + donor.getFrequency() * (1 - weight)).reversed())
					.collect(Collectors.toList());

			List<Donor> donorsToCall = donors.stream().limit(numberOfDonors).collect(Collectors.toList());
			int numberOfRespondedDonors = getApproximateNumber(donorsToCall);
			int r = maxBloodLevel - minBloodLevel;
			int x = bloodLevel + numberOfRespondedDonors - consumption;
			double rThird = r / (double) 3;
			double rTwoThird = 2 * r / (double) 3;

			if (x <= minBloodLevel) {
				totalLoss += 200 / (double) r * (minBloodLevel - x) + 35;
			} else if (x > minBloodLevel && x <= rThird + minBloodLevel) {
				totalLoss += 100 / (double) r * (minBloodLevel + rThird - x);
			} else if (rThird + minBloodLevel < x && x <= rTwoThird + minBloodLevel) {
				totalLoss += 0;
			} else if (rTwoThird + minBloodLevel < x && x <= maxBloodLevel) {
				totalLoss += 100 / (double) r * (x - rTwoThird - minBloodLevel);
			} else {
				totalLoss += 200 / (double) r * (x - maxBloodLevel) + 35;
			}
		}

		return totalLoss;
	}

	private int getApproximateNumber(List<Donor> donorsToCall) {
		int callHim = 0;

		for (Donor donor : donorsToCall) {
			double nextProbability = random.nextDouble();

			double sexProbability;
			if (donor.getSex().equals("M")) {
				sexProbability = donor.getFrequency() / 4.2;
			} else {
				sexProbability = donor.getFrequency() / 3.2;
			}
			double distanceProbability = donor.getDistance() / 32;

			if (nextProbability < distanceProbability * sexProbability) {
				callHim++;
			}
		}

		return callHim;
	}

	@Override
	public int getNumberOfSolutionVariables() {
		return NUM_OF_BLOOD_TYPES + 1;
	}
}