package hackathon.dekoratori.algorithm.genetic;

public enum BloodType {
	OM, OP, AM, AP, BM, BP, ABM, ABP;

	public static BloodType getByString(String name) {
		switch (name) {
			case "0+":
				return OM;
			case "0-":
				return OP;
			case "A+":
				return AP;
			case "A-":
				return AM;
			case "B+":
				return BP;
			case "B-":
				return BM;
			case "AB+":
				return ABP;
			case "AB-":
				return ABM;

		}
		return null;
	}
}
