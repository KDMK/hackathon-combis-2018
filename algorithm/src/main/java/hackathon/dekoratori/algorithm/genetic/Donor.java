package hackathon.dekoratori.algorithm.genetic;

import lombok.Data;

@Data
public class Donor {

	private String id;
	private Integer last_donation;
	private Double frequency;
	private String  blood_group;
	private Double distance;
	private String sex;

	private double probability;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getLast_donation() {
		return last_donation;
	}

	public void setLast_donation(Integer last_donation) {
		this.last_donation = last_donation;
	}

	public Double getFrequency() {
		return frequency;
	}

	public void setFrequency(Double frequency) {
		this.frequency = frequency;
	}

	public String getBlood_group() {
		return blood_group;
	}

	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}
}