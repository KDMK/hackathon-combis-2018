package hackathon.dekoratori.algorithm.genetic;

public interface Evaluator {

	double evaluate(double[] solution);

	int getNumberOfSolutionVariables();
}
