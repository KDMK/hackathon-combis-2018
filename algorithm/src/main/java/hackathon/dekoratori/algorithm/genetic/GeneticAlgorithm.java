package hackathon.dekoratori.algorithm.genetic;

import hackathon.dekoratori.algorithm.genetic.geneticUtil.Crossover;
import hackathon.dekoratori.algorithm.genetic.geneticUtil.Mutation;
import hackathon.dekoratori.algorithm.genetic.geneticUtil.Selection;
import hackathon.dekoratori.algorithm.genetic.model.DoubleArrayPopulation;
import hackathon.dekoratori.algorithm.genetic.model.DoubleArraySolution;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class GeneticAlgorithm {

	private int maxIterations;

	private double acceptableFitness;

	private DoubleArrayPopulation population;

	private int currentIteration;

	private Evaluator evaluator;

	private DoubleArraySolution bestSolution = null;

	public GeneticAlgorithm(Evaluator evaluator, int maxIterations, double acceptableFitness, int popSize) {
		this.evaluator = evaluator;
		this.maxIterations = maxIterations;
		this.acceptableFitness = acceptableFitness;
		this.population = new DoubleArrayPopulation(popSize, evaluator.getNumberOfSolutionVariables());
	}

	public DoubleArraySolution run() {
		while (currentIteration++ < maxIterations) {
			while (currentIteration++ < maxIterations) {
				population.evaluate(evaluator);
				bestSolution = population.getBestSolutions(1).get(0);
				log.info(bestSolution.getFitness() + "");
				if (bestSolution.getFitness() < acceptableFitness) {
					break;
				}

				DoubleArrayPopulation newPopulation = new DoubleArrayPopulation();
				for (DoubleArraySolution goodSolution : population.getBestSolutions(2)) {
					newPopulation.addMember(goodSolution.newLikeThis());
				}

				while (newPopulation.size() < population.size()) {
					DoubleArraySolution firstParent = Selection.tournamentSelection(population, 5);
					DoubleArraySolution secondParent = Selection.tournamentSelection(population, 5);

					DoubleArraySolution first = Crossover.uniformCrossover(firstParent, secondParent);
					DoubleArraySolution second = Crossover.mixCrossover(firstParent, secondParent);
					Mutation.bloodTypeMutation(first);
					Mutation.bloodTypeMutation(second);
					newPopulation.addMember(first).addMember(second);
				}

				population = newPopulation;
			}

			return bestSolution;
		}

		return bestSolution;
	}
}