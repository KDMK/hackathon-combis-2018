package hackathon.dekoratori.algorithm.genetic.geneticUtil;

import hackathon.dekoratori.algorithm.genetic.model.DoubleArraySolution;

import java.util.Random;

public class Crossover {

	private static final double BLX_ALPHA = 0.2;

	public static DoubleArraySolution blxAlphaCrossover(DoubleArraySolution first, DoubleArraySolution second) {
		double[] firstValues = first.getSolution();
		double[] secondValues = second.getSolution();

		double[] newValues = new double[firstValues.length];
		Random random = new Random();
		for (int i = 0; i < firstValues.length; i++) {
			double min = firstValues[i] <= secondValues[i] ? firstValues[i] : secondValues[i];
			double max = firstValues[i] > secondValues[i] ? firstValues[i] : secondValues[i];
			double range = max - min;
			min -= range * BLX_ALPHA;
			max += range * BLX_ALPHA;
			range = max - min;
			newValues[i] = min + range * random.nextDouble();
		}

		return new DoubleArraySolution(newValues);
	}

	public static DoubleArraySolution mixCrossover(DoubleArraySolution first, DoubleArraySolution second) {
		double[] firstValues = first.getSolution();
		double[] secondValues = second.getSolution();

		double[] newValues = new double[firstValues.length];
		for (int i = 0; i < firstValues.length; i++) {
			if (i % 2 == 0) {
				newValues[i] = firstValues[i];
			} else {
				newValues[i] = secondValues[i];
			}
		}

		return new DoubleArraySolution(newValues);
	}

	public static DoubleArraySolution uniformCrossover(DoubleArraySolution first, DoubleArraySolution second) {
		double[] firstValues = first.getSolution();
		double[] secondValues = second.getSolution();

		double[] newValues = new double[firstValues.length];
		Random random = new Random();
		int border = random.nextInt(firstValues.length);
		for (int i = 0; i < firstValues.length; i++) {
			if (i < border) {
				newValues[i] = firstValues[i];
			} else {
				newValues[i] = secondValues[i];
			}
		}

		return new DoubleArraySolution(newValues);
	}
}