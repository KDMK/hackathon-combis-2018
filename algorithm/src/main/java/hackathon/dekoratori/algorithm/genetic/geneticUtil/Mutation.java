package hackathon.dekoratori.algorithm.genetic.geneticUtil;

import hackathon.dekoratori.algorithm.genetic.model.DoubleArraySolution;

import java.util.Random;

public class Mutation {

	private static final double MUTATION_PROBABILITY = 0.50;

	public static void normalDistributionMutator(DoubleArraySolution first, double sigma) {
		double[] values = first.getSolution();
		Random random = new Random();
		for (int i = 0; i < values.length; i++) {
			if (random.nextDouble() < MUTATION_PROBABILITY) {
				values[i] += random.nextGaussian() * sigma;
			}
		}
	}

	public static void bloodTypeMutation(DoubleArraySolution first) {
		double[] values = first.getSolution();
		Random random = new Random();
		for (int i = 0; i < values.length; i++) {
			if (random.nextDouble() < MUTATION_PROBABILITY) {
				if (i != values.length - 1) {
					values[i] += random.nextGaussian() * 0.05;
					if(values[i] < 1) {
						values[i] = 1;
					}
				} else {
					values[i] += random.nextGaussian() * 0.05;
					if (values[i] > 1) {
						values[i] = 1;
					} else if (values[i] < 0) {
						values[i] = 0;
					}
				}
			}
		}
	}
}
