package hackathon.dekoratori.algorithm.genetic.geneticUtil;


import hackathon.dekoratori.algorithm.genetic.model.DoubleArrayPopulation;
import hackathon.dekoratori.algorithm.genetic.model.DoubleArraySolution;

public class Selection {

	public static DoubleArraySolution tournamentSelection(DoubleArrayPopulation population, int numberOfElements) {
		DoubleArrayPopulation tournamentPopulation = new DoubleArrayPopulation();
		tournamentPopulation.setMembers(population.getRandomSolutions(numberOfElements));
		return tournamentPopulation.getBestSolutions(numberOfElements).get(1);
	}

}