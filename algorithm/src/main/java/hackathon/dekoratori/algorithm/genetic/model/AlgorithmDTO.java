package hackathon.dekoratori.algorithm.genetic.model;

import hackathon.dekoratori.algorithm.genetic.Donor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class AlgorithmDTO {

    private List<Donor> csvBloodData;

    private Map<String, Long> bloodSupply;

    public AlgorithmDTO() {
    }

    public List<Donor> getCsvBloodData() {
        return csvBloodData;
    }

    public void setCsvBloodData(List<Donor> csvBloodData) {
        this.csvBloodData = csvBloodData;
    }

    public Map<String, Long> getBloodSupply() {
        return bloodSupply;
    }

    public void setBloodSupply(Map<String, Long> bloodSupply) {
        this.bloodSupply = bloodSupply;
    }
}
