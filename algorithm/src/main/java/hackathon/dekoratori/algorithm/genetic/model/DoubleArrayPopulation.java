package hackathon.dekoratori.algorithm.genetic.model;


import hackathon.dekoratori.algorithm.genetic.Evaluator;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class DoubleArrayPopulation {

	private List<DoubleArraySolution> members;

	private Random random = new Random();

	public DoubleArrayPopulation() {
		members = new ArrayList<>();
	}

	public DoubleArrayPopulation(int populationSize, int solutionSize) {
		createPopulation(populationSize, solutionSize);
	}

	public List<DoubleArraySolution> getMembers() {
		return members;
	}

	public void setMembers(List<DoubleArraySolution> members) {
		this.members = members;
		this.members.sort(Comparator.comparingDouble(DoubleArraySolution::getFitness).reversed());
	}

	public DoubleArrayPopulation addMember(DoubleArraySolution solution) {
		members.add(solution);
		return this;
	}

	public void removeMember(DoubleArraySolution solution) {
		members.remove(solution);
	}

	public int size() {
		return members.size();
	}

	public List<DoubleArraySolution> getBestSolutions(int numberOfBest) {
		members.sort(Comparator.comparingDouble(DoubleArraySolution::getFitness));
		List<DoubleArraySolution> solutions = new LinkedList<>();
		int counter = 0;
		while (numberOfBest-- > 0) {
			solutions.add(members.get(counter++));
		}

		return solutions;
	}

	public List<DoubleArraySolution> getRandomSolutions(int numberOfBest) {
		List<DoubleArraySolution> solutions = new LinkedList<>();

		while (numberOfBest-- > 0) {
			int randomIndex = random.nextInt(members.size());
			solutions.add(members.get(randomIndex));
		}

		return solutions;
	}

	public void evaluate(Evaluator evaluator) {
		for (DoubleArraySolution member : members) {
			member.setFitness(evaluator.evaluate(member.getSolution()));
		}

		members.sort(Comparator.comparingDouble(DoubleArraySolution::getFitness));
	}

	private void createPopulation(int populationSize, int solutionSize) {
		members = new ArrayList<>();
		while (populationSize-- > 0) {
			members.add(new DoubleArraySolution(solutionSize));
		}
	}
}