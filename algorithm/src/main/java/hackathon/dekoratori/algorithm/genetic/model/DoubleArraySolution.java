package hackathon.dekoratori.algorithm.genetic.model;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class DoubleArraySolution {

	private static final int MIN = 3;

	private static final int MAX = 7;

	private double[] solution;

	private double fitness;

	public DoubleArraySolution(int solutionSize) {
		createRandomSolution(solutionSize);
	}

	public DoubleArraySolution(double[] solution) {
		this.solution = solution;
	}

	public double[] getSolution() {
		return solution;
	}

	public void setSolution(double[] solution) {
		this.solution = solution;
	}

	public double getFitness() {
		return fitness;
	}

	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	public int getSize() {
		return solution.length;
	}

	public DoubleArraySolution newLikeThis() {
		DoubleArraySolution newSolution = new DoubleArraySolution(Arrays.copyOf(solution, solution.length));
		newSolution.fitness = fitness;

		return newSolution;
	}

	private void createRandomSolution(int solutionSize) {
		Random random = ThreadLocalRandom.current();
		solution = new double[solutionSize];

		for (int i = 0; i < solutionSize; i++) {
			if (i != solutionSize - 1) {
				solution[i] = 3.0 + random.nextGaussian() * 1;
				if (solution[i] < 1) {
					solution[i] = 1;
				}
			} else {
				solution[i] = 0.5 + random.nextGaussian() * 0.2;
			}
		}
	}

	@Override
	public String toString() {
		return "Current fitness : " + fitness + " Values " + Arrays.toString(solution);
	}
}