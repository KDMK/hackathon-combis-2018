package hackathon.dekoratori.algorithm.rest;

import hackathon.dekoratori.algorithm.constant.Constants;
import hackathon.dekoratori.algorithm.service.EvaluatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TestController {

	@Autowired
	private Trainer trainer;

	@Autowired
	private Constants constants;

	@Autowired
	private EvaluatorService evaluatorService;

	@GetMapping("/constants")
	public Constants getConstants() {
		return constants;
	}

	@PostMapping("/test")
	public ArrayList<Long> get(@RequestBody List<Long> ids) {
		return new ArrayList<>(Arrays.asList(1L, 2L, 3L));
	}

	@GetMapping("/test")
	public void test() {
		evaluatorService.evaluateDonors(new ArrayList<>());
	}

	@GetMapping("/train")
	public void train() {
		trainer.train();
	}

	@GetMapping("/donorsToCall")
	public List<Integer> getDonorsToCall() {
		return evaluatorService.getDonorsToCall().stream().map(Integer::parseInt).collect(Collectors.toList());
	}

	@GetMapping("/update")
	public int[] updateSupplies() {
		return evaluatorService.updateSupplies();
	}
}