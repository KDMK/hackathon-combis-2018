package hackathon.dekoratori.algorithm.service;

import hackathon.dekoratori.algorithm.TheAlgorithm;
import hackathon.dekoratori.algorithm.genetic.BloodType;
import hackathon.dekoratori.algorithm.genetic.Donor;
import hackathon.dekoratori.algorithm.genetic.model.AlgorithmDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EvaluatorService {

	private static final String EVALUATOR_URL = "http://hackaton.westeurope.cloudapp.azure.com/api/evaluate";

	private static final String DONORS_URL = "http://localhost:8080/api/algorithm";

	@Autowired
	private TheAlgorithm algorithm;

	@Autowired
	private hackathon.dekoratori.algorithm.constant.Constants constants;

	@Autowired
	private RestTemplate restTemplate;

	public List<String> getDonorsToCall() {
		int[] levels = new int[]{50,130,60,150,50,30,8,20};
		algorithm.init(getDonors(), levels);
		return algorithm.getBloodDonors().stream().map(Donor::getId).collect(Collectors.toList());
	}

	public Integer evaluateDonors(List<Donor> donors) {
		donors = getDonors();
		ArrayList<Long> sentIds = new ArrayList<>(getDonors().stream().map(dono -> Long.parseLong(dono.getId())).collect(Collectors.toList()));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Object> entity = new HttpEntity<>(new Parameters(sentIds, 7), headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(EVALUATOR_URL, HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {
		});
		String[] split = responseEntity.getBody().split(",");
		List<Donor> donorList = new ArrayList<>();
		for (String aSplit : split) {
			for (Donor donor : donors) {
				if (donor.getId().equals(aSplit)) {
					donorList.add(donor);
					break;
				}
			}
		}
		return split.length;
	}

	public List<Donor> getDonors() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Object> entity = new HttpEntity<>(headers);
		ResponseEntity<AlgorithmDTO> responseEntity = restTemplate.exchange(DONORS_URL, HttpMethod.GET, entity, new ParameterizedTypeReference<AlgorithmDTO>() {
		});
		return responseEntity.getBody().getCsvBloodData();
	}

	public int[] updateSupplies() {
		List<Donor> donors = getDonors();
		Path path = Paths.get("supplies.txt");
		String supplies = null;
		try {
			supplies = Files.readAllLines(path).get(0);
		} catch (IOException e) {
		}

		String[] parts = supplies.split(",");
		int[] supp = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			String part = parts[i];
			supp[i] = Integer.parseInt(part);
		}
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get("current.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Map<BloodType, Integer> map = new HashMap<>();
		String[] ids = lines.get(0).split(",");
		for (String id : ids) {
			for (Donor donor : donors) {
				if (donor.getId().equals(id)) {
					BloodType type = BloodType.getByString(donor.getBlood_group());
					map.putIfAbsent(type, 0);
					map.put(type, map.get(type) + 1);
				}
			}
		}

		int[] newSupp = new int[parts.length];
		newSupp[0] = supp[0] + map.get(BloodType.OM) - constants.getLimits().get(BloodType.OM).getConsumption();
		newSupp[1] = supp[1] + map.get(BloodType.OP) - constants.getLimits().get(BloodType.OP).getConsumption();
		newSupp[2] = supp[2] + map.get(BloodType.AM) - constants.getLimits().get(BloodType.AM).getConsumption();
		newSupp[3] = supp[3] + map.get(BloodType.AP) - constants.getLimits().get(BloodType.AP).getConsumption();
		newSupp[4] = supp[4] + map.get(BloodType.BM) - constants.getLimits().get(BloodType.BM).getConsumption();
		newSupp[5] = supp[5] + map.get(BloodType.BP) - constants.getLimits().get(BloodType.BP).getConsumption();
		newSupp[6] = supp[6] + map.get(BloodType.ABM) - constants.getLimits().get(BloodType.ABM).getConsumption();
		newSupp[7] = supp[7] + map.get(BloodType.ABP) - constants.getLimits().get(BloodType.ABP).getConsumption();
		return newSupp;
	}

	@AllArgsConstructor
	@Data
	private class Parameters {
		private List<Long> input_ids;

		private int days_past = 7;
	}
}
