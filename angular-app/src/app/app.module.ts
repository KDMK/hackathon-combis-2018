import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';

import {AppComponent} from './component/app/app.component';
import {MaterialModule} from "./material.module";
import {GlobalErrorHandlerService} from "./service/global-error-handler.service";
import {HttpClientModule} from "@angular/common/http";
import {AppRouterModule} from "./service/app-router.module";
import {FormsModule} from "@angular/forms";
import {ToasterModule} from "angular5-toaster/dist";
import {HomeComponent} from "./component/home/home.component";
import {BloodSupplyComponent} from "./component/bloodSupply/edit-bloodSupply.component";
import {BloodSupplyNewDialog} from "./component/bloodSupply/bloodSupply-new.dialog";
import {DonorProfileComponent} from "./component/donor-profile/donor-profile.component";
import {BloodDonorTableComponent} from "./component/bloodDonor/blood-donor-table/blood-donor-table.component";
import {NotificationComponent} from "./component/notification/notification/notification.component";
import {PollComponent} from "./component/poll/poll/poll.component";
import {AdditionalDataComponent} from "./component/additionalData/additional-data/additional-data.component";
import {PollNewDialog} from "./component/poll/poll/poll-new.dialog";
import {QRCodeModule} from "angularx-qrcode";
import {QrcodeComponent} from "./component/qrcode/qrcode.component";
import {AuthService} from "./service/auth.service";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BloodSupplyComponent,
    BloodSupplyNewDialog,
    DonorProfileComponent,
    BloodSupplyNewDialog,
    BloodDonorTableComponent,
    NotificationComponent,
    PollComponent,
    PollNewDialog,
    AdditionalDataComponent,
    QrcodeComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    AppRouterModule,
    FormsModule,
    ToasterModule,
    QRCodeModule
  ],
  providers: [
    AuthService,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandlerService
    }
  ],
  entryComponents: [
    BloodSupplyNewDialog,
    PollNewDialog
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
