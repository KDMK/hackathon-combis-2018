import {Component, OnInit} from '@angular/core';
import {AdditionalDataServiceService} from "../../../service/additional-data-service.service";
import {AdditionalData} from "../../../domain/AdditionalData";
import {HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-additional-data',
  templateUrl: './additional-data.component.html',
  styleUrls: ['./additional-data.component.css']
})
export class AdditionalDataComponent implements OnInit {

  additionalData : AdditionalData[];

  constructor(private additionalDataService : AdditionalDataServiceService) { }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.additionalDataService.getAdditionalDataForDonor(1).toPromise()
      .then((response) => {
        this.additionalData = (<HttpResponse<AdditionalData[]>>response).body;
      }).catch((err) => {
      console.error(err);
    })
  }
}
