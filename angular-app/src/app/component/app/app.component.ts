import { Component } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {Observable} from "rxjs/Observable";
import {LoaderService} from "../../service/loader.service";
import {Router} from "@angular/router";
import {AuthService} from "../../service/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public toasterconfig: ToasterConfig =
    new ToasterConfig({
      showCloseButton: true,
      tapToDismiss: false,
      timeout: 0
    });

  public isLoading: Observable<boolean>;

  constructor(private loaderService: LoaderService, private toaster: ToasterService, private router: Router, private auth: AuthService) {
    this.isLoading = this.loaderService.getLoadingState();
    // auth.getLoggedInUser()
  }

  popToast(): void {
    this.toaster.pop("success", "Uspjeh!", "Test notifikacije");
  }

  btnUsers() {
    this.router.navigateByUrl("/users")
  }

  btnNotifications() {
    this.router.navigateByUrl("/notifications")
  }

  btnPolls() {
    this.router.navigateByUrl("/polls")
  }

  btnAdditionalData() {
    this.router.navigateByUrl("/additional-data")
  }
}
