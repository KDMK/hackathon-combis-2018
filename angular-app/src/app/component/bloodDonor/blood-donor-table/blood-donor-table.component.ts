import { Component, OnInit } from '@angular/core';
import {BloodDonor} from "../../../domain/BloodDonor";
import {BloodDonorService} from "../../../service/bloodDonor.service";
import {MatTableDataSource} from "@angular/material";
import {HttpResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-blood-donor-table',
  templateUrl: './blood-donor-table.component.html',
  styleUrls: ['./blood-donor-table.component.css']
})
export class BloodDonorTableComponent implements OnInit {

  constructor(private bloodDonorService: BloodDonorService, private router: Router) { }

  displayedColumns = [
    "name",
    "bloodType",
    "gender",
    "canDonate"
  ];

  dataSource: MatTableDataSource<BloodDonor>;
  donors: BloodDonor[];

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.donors);
    this.loadData();
  }

  canDonate(donor: BloodDonor) {
    let lastDonated = new Date(donor.lastDonation);
    let daysBetweenDonations = (donor.gender == "M") ? 90 : 120;
    if(this.inBetween(lastDonated, new Date()) > daysBetweenDonations) {
      return "YES";
    }
    return "NO";
  }

  public openDonorProfile(id: number) {
    this.router.navigate(['donor-profile'])
  }


  public inBetween(date1, date2) {
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms/one_day);
  }

  private loadData() {
    this.bloodDonorService.getAllDonors().toPromise()
      .then((response) => {
        this.donors = (<HttpResponse<BloodDonor[]>>response).body;
        this.dataSource.data = this.donors;
      }).catch((err) => {
      console.error(err);
    })
  }
}
