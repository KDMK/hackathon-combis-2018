import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {BloodRequest} from "../../domain/BloodRequest";

@Component({
  selector: "dialog-blood-supply-new",
  templateUrl: "./bloodSupply-new.dialog.html",
  styleUrls: ["./edit-bloodSupply.component.css"]
})
export class BloodSupplyNewDialog {
  public bloodRequest: BloodRequest;

  constructor(public dialogRef: MatDialogRef<BloodSupplyNewDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.bloodRequest = Object.assign({}, data.category);
    console.log(data)
  }

  closeDialog() {
    this.dialogRef.close(null);
  }
}
