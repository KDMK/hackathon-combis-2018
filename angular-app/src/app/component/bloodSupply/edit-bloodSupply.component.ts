import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpResponse} from "@angular/common/http";
import {BloodSupplyNewDialog} from "./bloodSupply-new.dialog";
import {MatDialog, MatSort, MatTableDataSource} from "@angular/material";
import {ToasterService} from "angular5-toaster/dist";
import {BloodSupply} from "../../domain/BloodSupply";
import {BloodSupplyService} from "../../service/bloodSupply.service";

@Component({
  selector: 'app-blood-supply',
  templateUrl: './edit-bloodSupply.component.html',
  styleUrls: ['./edit-bloodSupply.component.css']
})
export class BloodSupplyComponent implements OnInit {
  private readonly BLOOD_REQUEST = "Pošalji zahtjev za prikupljanje krvi";

  @ViewChild(MatSort) sort: MatSort;

  public bloodSupplies: BloodSupply[];
  public dataSource: MatTableDataSource<BloodSupply>;
  displayedColumns = ['bloodType', 'amount'];

  constructor(private bloodSupplyService: BloodSupplyService, private dialog: MatDialog, private toasterService: ToasterService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.bloodSupplies);
    this.dataSource.sort = this.sort;
    this.refreshBloodSupplies();
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  private refreshBloodSupplies() {
    this.bloodSupplyService.getAllBloodSupply().toPromise().then((response) => {
      this.bloodSupplies = (<HttpResponse<BloodSupply[]>>response).body;
      this.bloodSupplies.forEach(t => {
        if((<any> t).bloodType.indexOf('P') != -1) {
          (<any> t).bloodType = (<any> t).bloodType.replace('P', '+');
        } else {
          (<any> t).bloodType = (<any> t).bloodType.replace('M', '-');
        }
      });
      this.dataSource.data = this.bloodSupplies;
    }).catch((err) => {
      console.error(err);
    })
  }

  public openNewDialog() {
    const dialogRef = this.dialog.open(BloodSupplyNewDialog, {
      width: "500px",
      data: {
        title: this.BLOOD_REQUEST,
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      if(result == null || Object.keys(result).length == 0) return;
      this.bloodSupplyService.bloodSupplyRequest(result).toPromise().then((response) => {
        this.toasterService.pop("success", "Uspješno je", "predan zahtjev za prikupljanjem krvi")
        this.refreshBloodSupplies();
      });
    })
  }

}
