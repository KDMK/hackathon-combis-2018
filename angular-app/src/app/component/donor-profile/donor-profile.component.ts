import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTableDataSource} from "@angular/material";
import {ToasterService} from "angular5-toaster/dist";
import {BloodDonation} from "../../domain/BloodDonation";

@Component({
  selector: 'app-donor-profile',
  templateUrl: './donor-profile.component.html',
  styleUrls: ['./donor-profile.component.css']
})
export class DonorProfileComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;

  public bloodDonations: BloodDonation[];
  public dataSource: MatTableDataSource<BloodDonation>;
  displayedColumns = ['ord', 'date', 'location'];

  constructor(
    // private bloodDonorService: BloodDonorService,
       private dialog: MatDialog, private toasterService: ToasterService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.bloodDonations);
    this.dataSource.sort = this.sort;
    this.refreshBloodSupplies();
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  private refreshBloodSupplies() {
    const data = [
      {
        "ord": 1,
        "date": new Date(2018, 4, 12),
        "location": "Petrova 3"
      },
      {
        "ord": 2,
        "date": new Date(2018, 1, 12),
        "location": "Petrova 3"
      },
      {
        "ord": 3,
        "date": new Date(2017, 10, 12),
        "location": "Petrova 3"
      },
    ];
    this.bloodDonations = data;
    console.log(this.bloodDonations);
    this.dataSource.data = this.bloodDonations;
    // this.bloodDonorService.getAllBloodSupply().toPromise().then((response) => {
    //   this.bloodSupplies = (<HttpResponse<BloodSupply[]>>response).body;
    //   this.dataSource.data = this.bloodSupplies;
    // }).catch((err) => {
    //   console.error(err);
    // })
  }

}
