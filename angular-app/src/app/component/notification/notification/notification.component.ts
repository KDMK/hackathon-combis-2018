import {Component, OnInit} from '@angular/core';
import {HttpResponse} from "@angular/common/http";
import {NotificationServiceService} from "../../../service/notification-service.service";
import {Notification} from "../../../domain/Notification";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  notifications : Notification[];

  constructor(private notificationService : NotificationServiceService) { }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.notificationService.getAllNotifications()
      .then((response) => {
        this.notifications = response;
      }).catch((err) => {
      console.error(err);
    })
  }
}
