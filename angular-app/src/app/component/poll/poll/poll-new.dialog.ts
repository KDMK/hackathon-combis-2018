import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Poll} from "../../../domain/Poll";

@Component({
  selector: "dialog-poll-new",
  templateUrl: "./poll-new.dialog.html",
  styleUrls: ["./poll.component.css"]
})
export class PollNewDialog {
  public newPoll: Poll;

  constructor(public dialogRef: MatDialogRef<PollNewDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.newPoll = Object.assign({}, data.category);
    console.log(data)
  }

  closeDialog() {
    this.dialogRef.close(null);
  }
}
