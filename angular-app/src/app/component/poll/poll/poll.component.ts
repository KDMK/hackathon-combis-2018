import {Component, OnInit, ViewChild} from '@angular/core';
import {Poll} from "../../../domain/Poll";
import {PollServiceService} from "../../../service/poll-service.service";
import {HttpResponse} from "@angular/common/http";
import {BloodSupply} from "../../../domain/BloodSupply";
import {ConfirmDialog} from "../../shared/confirm.dialog";
import {MatDialog, MatSort, MatTableDataSource} from "@angular/material";
import {ToasterService} from "angular5-toaster/dist";
import {PollNewDialog} from "./poll-new.dialog";

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit {
  private readonly POLL_CREATE = "Stvori novo pitanje";
  private readonly POLL_EDIT = "Uredi pitanje";

  @ViewChild(MatSort) sort: MatSort;

  public dataSource: MatTableDataSource<Poll>;
  displayedColumns = ['name', 'isCritical'];

  public polls: Poll[];

  constructor(private pollService: PollServiceService, private dialog: MatDialog, private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.polls);
    this.loadData();
  }

  private loadData() {
    this.pollService.getAllPolls().toPromise()
      .then((response) => {
        this.polls = (<HttpResponse<Poll[]>>response).body;
        this.dataSource.data = this.polls;
      }).catch((err) => {
      console.error(err);
    })
  }

  private refreshPolls() {
    this.pollService.getAllPolls().toPromise().then((response) => {
      this.polls = (<HttpResponse<Poll[]>>response).body;
      this.dataSource.data = this.polls;
    }).catch((err) => {
      console.error(err);
    })
  }

  public openNewDialog() {
    const dialogRef = this.dialog.open(PollNewDialog, {
      width: "500px",
      data: {
        title: this.POLL_CREATE,
        parentCategories: this.polls,
        category: new BloodSupply()
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == null || Object.keys(result).length == 0) return;
      this.pollService.savePoll(result).toPromise().then((response) => {
        this.toasterService.pop("success", "Uspješno je", `dodano novo pitanje: ${result.name}`);
        this.refreshPolls();
      });
    })
  }

  public openEditDialog(poll: Poll) {
    const dialogRef = this.dialog.open(PollNewDialog, {
      width: "500px",
      data: {
        title: this.POLL_EDIT,
        poll: poll
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == null || Object.keys(result).length == 0) return;
      this.pollService.editPoll(result).toPromise().then((response) => {
        this.toasterService.pop("success", "Uspješno je", `uređeno pitanje: ${result.name}`);
        this.refreshPolls();
      });
    })
  }

  public deletePoll(poll: Poll) {
    const dialogRef = this.dialog.open(ConfirmDialog, {
      width: "500px",
      data: {
        title: "Jeste li sigurni?",
        message: "Pitanje će biti trajno uklonjeno i nece biti moguće poništiti rezultat ove akcije."
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == null) return;
      this.pollService.editPoll(result).toPromise().then((response) => {
        this.pollService.deletePoll(poll).toPromise().then((result) => {
          this.toasterService.pop("success", "Uspješno je", `obrisana kategorija: ${poll.name}`);
          this.refreshPolls();
        })
      });
    })
  }
}
