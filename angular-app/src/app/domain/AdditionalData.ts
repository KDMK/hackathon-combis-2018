import {Poll} from "./Poll";
import {BloodDonor} from "./BloodDonor";

export class AdditionalData {
  id: number;
  poll : Poll;
  bloodDonor : BloodDonor;
}
