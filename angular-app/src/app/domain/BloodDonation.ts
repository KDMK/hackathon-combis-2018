export class BloodDonation {
  ord: Number;
  date: Date;
  location: String;

  constructor(ord, date, location) {
    this.ord = ord;
    this.date = date;
    this.location = location;
  }
}
