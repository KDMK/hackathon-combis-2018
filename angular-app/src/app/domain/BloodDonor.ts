export class BloodDonor {
  id: number;
  userId: number;
  firstName: string;
  lastName: string;
  gender: string;
  bloodType: string;
  lastDonation: string;
  frequency: number;
}
