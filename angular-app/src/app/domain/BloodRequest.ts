export class BloodRequest {
  amount: Number;
  bloodType: String;

  constructor(ord, date) {
    this.amount = ord;
    this.bloodType = date;
  }
}
