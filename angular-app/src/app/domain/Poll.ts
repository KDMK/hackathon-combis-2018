export class Poll {
  id: number;
  name : string;
  isCritical : boolean;
}
