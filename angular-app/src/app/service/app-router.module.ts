import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "../component/home/home.component";
import {BloodSupplyComponent} from "../component/bloodSupply/edit-bloodSupply.component";
import {BloodDonorTableComponent} from "../component/bloodDonor/blood-donor-table/blood-donor-table.component";
import {DonorProfileComponent} from "../component/donor-profile/donor-profile.component";
import {NotificationComponent} from "../component/notification/notification/notification.component";
import {PollComponent} from "../component/poll/poll/poll.component";
import {AdditionalDataComponent} from "../component/additionalData/additional-data/additional-data.component";
import {QrcodeComponent} from "../component/qrcode/qrcode.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'blood-supplies', component: BloodSupplyComponent },
  { path: 'donor-profile', component: DonorProfileComponent },
  { path: 'users', component: BloodDonorTableComponent},
  { path: 'notifications', component: NotificationComponent},
  { path: 'polls', component: PollComponent},
  { path: 'additional-data', component: AdditionalDataComponent},
  { path: 'qr-generator', component: QrcodeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRouterModule {


}
