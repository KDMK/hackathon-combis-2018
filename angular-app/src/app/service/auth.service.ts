import {Injectable, OnInit} from "@angular/core";
import {HttpClient, HttpRequest} from "@angular/common/http";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

class Role {
  authority: String;

  constructor(auth) {
    this.authority = auth;
  }
}

class User {
  username: String;
  roles: Array<Role>;
  firstName: String;
  lastName: String;
  email: String;
}

@Injectable()
export class AuthService implements OnInit {
  private rolesList: Array<Role>;
  private roles: BehaviorSubject<Array<Role>>;

  constructor(private http: HttpClient) {
    this.rolesList = [new Role("ROLE_USER")];
    this.roles = new BehaviorSubject<Array<Role>>(this.rolesList);
  }

  getLoggedInUser() {
    console.log("Constructor")
    this.http.get("http://localhost:8080/api/users/logged").toPromise().then((p: Array<Role>) => {
      this.roles.next(p);
    });
  }

  isAdmin() {
    for(let role in this.rolesList) {
      if(role == "ROLE_ADMIN") return true;
    }
    return false;
  }

  ngOnInit(): void {
    this.getLoggedInUser();
  }

}
