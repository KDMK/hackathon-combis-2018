import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpEvent, HttpEventType, HttpRequest} from "@angular/common/http";
import {catchError, delay, last, map, tap} from "rxjs/operators";
import {LoaderService, State} from "./loader.service";

@Injectable({
  providedIn: 'root'
})
export class BloodDonorService {

  private readonly API_ENDPOINT = `${environment.api.prefix.api}${environment.api.endpoint.api.bloodDonor}`;

  constructor(private http: HttpClient, private loaderService: LoaderService) { }

  public getAllDonors() {
    const req = new HttpRequest("GET", this.API_ENDPOINT, {
      reportProgress: true,
      observe: 'events'
    });

    return this.http.request(req);
  }

  private getEventMessage(event: HttpEvent<any>, payload: any) {
    switch (event.type) {
      case HttpEventType.Sent:
        return event;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        return Math.round(100 * event.loaded / event.total);

      case HttpEventType.DownloadProgress:
        // Compute and show the % done: --> cannot calculate this because total size isn't known
        return event;

      case HttpEventType.Response:
        this.loaderService.setState(State.LOADED);
        return event;

      default:
        return event;
    }
  }

  private showProgress(message: any) {
    if(typeof message === 'number') {
      // notify progress bar... maybe...
    }
  }

  private handleError(event: Event | undefined) {
    return undefined;
  }
}
