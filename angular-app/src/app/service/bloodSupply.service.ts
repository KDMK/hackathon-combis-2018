import {Injectable} from "@angular/core";
import {HttpClient, HttpEvent, HttpEventType, HttpRequest} from "@angular/common/http";
import {environment} from "../../environments/environment"
// import {Observable} from "rxjs/internal/Observable";
import {catchError, delay, last, map, tap} from "rxjs/operators";
import {LoaderService, State} from "./loader.service";
import {BloodSupply} from "../domain/BloodSupply";
import {BloodRequest} from "../domain/BloodRequest";

@Injectable({
  providedIn: 'root'
})
export class BloodSupplyService {
  private readonly API_ENDPOINT = `${environment.api.prefix.api}${environment.api.endpoint.api.bloodSupply}`;

  constructor(private http: HttpClient, private loaderService: LoaderService) {
  }

  public getAllBloodSupply() {
    const req = new HttpRequest("GET", this.API_ENDPOINT, {
      reportProgress: true,
      observe: 'events'
    });

    return this.http.request(req).pipe(
      map(event => this.getEventMessage(event, null)),
      tap(message => this.showProgress(message)),
      delay(100),
      last(),
      catchError(this.handleError(event))
    );
  }

  public getBloodSupply(id: Number) {
    return this.http.get<BloodSupply>(`${this.API_ENDPOINT}/${id}`, {
      reportProgress: true,
      headers: {}
    });
  }

  public saveBloodSupply(bloodSupply: BloodSupply) {
    return this.http.post<BloodSupply>(`${this.API_ENDPOINT}`, bloodSupply, {
      reportProgress: true,
      headers: {}
    });
  }

  public editBloodSupply(bloodSupply: BloodSupply) {
    return this.http.put<BloodSupply>(`${this.API_ENDPOINT}`, bloodSupply, {
      reportProgress: true,
      headers: {}
    });
  }

  public deleteCategory(bloodSupply: BloodSupply) {
    return this.http.delete<BloodSupply>(`${this.API_ENDPOINT}/${bloodSupply.id}`, {
      reportProgress: true,
      headers: {}
    });
  }

  public bloodSupplyRequest(request: BloodRequest) {
    return this.http.post<BloodSupply>(`${this.API_ENDPOINT}/supply`, request, {
      reportProgress: true,
      headers: {}
    });
  }

  private getEventMessage(event: HttpEvent<any>, payload: any) {
    switch (event.type) {
      case HttpEventType.Sent:
        return event;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        return Math.round(100 * event.loaded / event.total);

      case HttpEventType.DownloadProgress:
        // Compute and show the % done: --> cannot calculate this because total size isn't known
        return event;

      case HttpEventType.Response:
        this.loaderService.setState(State.LOADED);
        return event;

      default:
        return event;
    }
  }

  private showProgress(message: any) {
    if(typeof message === 'number') {
      // notify progress bar... maybe...
    }
  }

  private handleError(event: Event | undefined) {
    return undefined;
  }
}
