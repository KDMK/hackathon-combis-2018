import {ErrorHandler, Injectable} from "@angular/core";
import {LoaderService, State} from "./loader.service";
import {ToasterService} from "angular5-toaster/dist";

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {

  constructor(private loaderService: LoaderService, private toasterService: ToasterService) {
  }

  handleError(error) {
    this.loaderService.setState(State.LOADED);
    this.toasterService.pop("error", "Dogodila se greška", error.message)

    throw error;
  }
}
