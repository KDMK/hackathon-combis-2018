import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";
import {delay} from "rxjs/operators";

export enum State {
  LOADING,
  LOADED
}

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private isLoading: boolean;
  private readonly subject: BehaviorSubject<boolean>;

  constructor() {
    this.isLoading = false;
    this.subject = new BehaviorSubject<boolean>(false);
  }

  public setState(state: State) {
    this.isLoading = state == State.LOADING;
    this.subject.next(this.isLoading);
  }

  public getLoadingState(): Observable<boolean> {
    return this.subject.asObservable().pipe(delay(10));
  }

}
