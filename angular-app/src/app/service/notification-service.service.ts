import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpEventType, HttpRequest} from "@angular/common/http";
import {LoaderService, State} from "./loader.service";
import {catchError, delay, last, map, tap} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {Notification} from "../domain/Notification";

@Injectable({
  providedIn: 'root'
})
export class NotificationServiceService {

  private readonly INFO_API_ENDPOINT = `${environment.api.prefix.api}${environment.api.endpoint.api.infoNotifications}`;
  private readonly DONATION_REQUEST_API_ENDPOINT = `${environment.api.prefix.api}${environment.api.endpoint.api.donationRequestNotifications}`;

  constructor(private http: HttpClient, private loaderService: LoaderService) { }

  public getAllNotifications() {
    const reqInfoNotification = new HttpRequest("GET", this.INFO_API_ENDPOINT, {
      reportProgress: true,
      observe: 'events'
    });

    const reqDonationRequest = new HttpRequest("GET", this.DONATION_REQUEST_API_ENDPOINT, {
      reportProgress: true,
      observe: 'events'
    });

    let notificationList: Notification[] = [];

    return this.http.request(reqInfoNotification).toPromise().then((data: any) => {
      notificationList = notificationList.concat(data.body);
      return this.http.request(reqDonationRequest).toPromise().then((data: any) => {
        return notificationList.concat(data.body);
      });
    });
  }

  private getEventMessage(event: HttpEvent<any>, payload: any) {
    switch (event.type) {
      case HttpEventType.Sent:
        return event;

      case HttpEventType.UploadProgress:
        // Compute and show the % done:
        return Math.round(100 * event.loaded / event.total);

      case HttpEventType.DownloadProgress:
        // Compute and show the % done: --> cannot calculate this because total size isn't known
        return event;

      case HttpEventType.Response:
        this.loaderService.setState(State.LOADED);
        return event;

      default:
        return event;
    }
  }

  private showProgress(message: any) {
    if(typeof message === 'number') {
      // notify progress bar... maybe...
    }
  }

  private handleError(event: Event | undefined) {
    return undefined;
  }
}
