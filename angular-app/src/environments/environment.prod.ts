export const environment = {
  production: false,
  api: {
    prefix: {
      api: "/api"
    },
    endpoint: {
      api:{
        bloodSupply: "/blood-supplies",
        bloodDonor: "/blood-donors",
        infoNotifications: "/info-notification",
        donationRequestNotifications: "/donation-request-notification",
        polls: "/poll-data",
        additionalData: "/additional-data"
      },
    }
  }
};
