package hr.fer.zavrsni.auth

import hr.fer.zavrsni.auth.controller.BloodDonorDto
import hr.fer.zavrsni.auth.controller.RegistrationDTO
import hr.fer.zavrsni.auth.email.EmailService
import hr.fer.zavrsni.auth.user.RoleRepository
import hr.fer.zavrsni.auth.user.User
import hr.fer.zavrsni.auth.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestMethod.POST
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.client.RestTemplate
import java.security.Principal
import javax.servlet.http.HttpServletRequest


@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan
@EnableAuthorizationServer
//@EnableWebMvc
@Controller
class AuthServer {

    @Autowired
    lateinit var tokenStore: TokenStore

    @Autowired
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var emailService: EmailService

    @Autowired
    lateinit var roleRepository: RoleRepository

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(AuthServer::class.java, *args)
        }
    }

    @RequestMapping("/success", method = [GET])
    fun success(): String = "success"

    @RequestMapping("/login", method = [GET])
    fun login(): String = "login"

    @RequestMapping("/register", method = [POST])
    fun register(@RequestBody registrationDTO: RegistrationDTO): String {
        val user = User(registrationDTO.email, registrationDTO.firstName,
                registrationDTO.lastName, bCryptPasswordEncoder.encode(registrationDTO.password),
                registrationDTO.email, listOf(roleRepository.findByRole("ROLE_USER")))
        userService.save(user)

        val restTemplate = RestTemplate()
        restTemplate.exchange("http://localhost:8080/api/blood-donors/registration", HttpMethod.POST, HttpEntity(registrationDTO), String::class.java)

        return "success"
    }

    @RequestMapping("/revoke-token", method = [GET])
    @ResponseStatus(HttpStatus.OK)
    private fun logout(request: HttpServletRequest) {
        val authHeader = request.getHeader("Authorization")
        if (authHeader != null) {
            val tokenValue = authHeader.replace("Bearer", "").trim({ it <= ' ' })
            val accessToken = tokenStore.readAccessToken(tokenValue)
            tokenStore.removeAccessToken(accessToken)
        }
    }

    @RequestMapping("/reset-password", method = [GET])
    @ResponseStatus(HttpStatus.OK)
    private fun resetPassword() {
       this.emailService.send();
    }
}


