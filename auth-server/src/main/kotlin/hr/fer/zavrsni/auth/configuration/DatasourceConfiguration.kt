package hr.fer.zavrsni.auth.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DriverManagerDataSource

@Configuration
@ConfigurationProperties(prefix = "app.datasource.auth")
class DatasourceConfiguration {

    @Bean(name = ["dataSource"])
    fun dataSource(): DriverManagerDataSource {
        val driverManagerDataSource = DriverManagerDataSource()
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver")
        driverManagerDataSource.url = "jdbc:postgresql://localhost:5432/testDB"
        driverManagerDataSource.username = "projektadmin"
        driverManagerDataSource.password = "projektadmin"
        driverManagerDataSource.schema = "kotlin_auth"
        return driverManagerDataSource
    }

}