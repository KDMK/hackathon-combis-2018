package hr.fer.zavrsni.auth.controller

data class BloodDonorDto(
        val userId: Long? = null,
        val firstName: String? = null,
        val lastName: String? = null,
        val gender: String? = null,
        val bloodType: String? = null
)
