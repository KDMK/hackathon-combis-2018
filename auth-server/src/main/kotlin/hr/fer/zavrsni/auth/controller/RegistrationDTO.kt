package hr.fer.zavrsni.auth.controller

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class RegistrationDTO(
        val password: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        val bloodType: String,
        val gender: String
) {
}
