package hr.fer.zavrsni.auth.email;

import javax.mail.MessagingException;

public interface EmailService {

    void send() throws MessagingException;
}
