package hr.fer.zavrsni.auth.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

@Service
    public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void send() throws MessagingException {
        final MimeMessage mailMessage = this.javaMailSender.createMimeMessage();
        mailMessage.setSubject("SPAM!");
        mailMessage.setContent("content", "text/html; charset=UTF-8");
        mailMessage.setSentDate(new Date());
        mailMessage.setFrom("bloodbank@no-reply.net");
        mailMessage.setRecipients(Message.RecipientType.TO, "matija.bartolac@gmail.com");
        this.javaMailSender.send(mailMessage);
    }
}
