package hr.fer.zavrsni.auth.user

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import org.codehaus.jackson.map.ext.JodaSerializers
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "users")
data class User(
        @Column(unique = true) val username: String,
        var firstName: String = "",
        var lastName: String = "",
        var password: String = "",
        @Column(unique = true) val email: String,
        @OneToMany(fetch = FetchType.EAGER)
        @JoinTable(
                name = "user_roles",
                joinColumns = [JoinColumn(name="user_id")],
                inverseJoinColumns = [JoinColumn(name="role_id")]
        )
        var roles: List<Role>,
        var active: Boolean? = true,
//        createdAt: LocalDateTime = LocalDateTime.now(),
//        var editedAt: LocalDateTime = LocalDateTime.now(),
        var resetToken: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0
)