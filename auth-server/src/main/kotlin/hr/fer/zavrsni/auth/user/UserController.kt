package hr.fer.zavrsni.auth.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/users")
class UserController {

    @Autowired
    lateinit var userService: UserService
    @Autowired
    lateinit var roleRepository: RoleRepository

    @GetMapping
    fun listUser(): List<User>? {
        return userService.findAll()
    }

    @GetMapping("/me")
    fun user(principal: Principal) = principal

    @GetMapping("/exists/{username}")
    fun userExists(@PathVariable username: String): Boolean {
        val user = userService.findUser(username)

        return user != null
    }

    @GetMapping("/{username}")
    fun getUser(@PathVariable username: String): User? {
        return userService.findUser(username)
    }

    @GetMapping("/roles/logged")
    fun logged(): Any? {
        val p = SecurityContextHolder.getContext().authentication.principal
        return userService.findUser((p as org.springframework.security.core.userdetails.User).username)
    }

}