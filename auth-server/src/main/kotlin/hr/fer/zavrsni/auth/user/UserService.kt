package hr.fer.zavrsni.auth.user

interface UserService {

    fun save(user: User): User
    fun findAll(): List<User>?
    fun delete(id: Long)
    fun findUser(username: String): User?
}