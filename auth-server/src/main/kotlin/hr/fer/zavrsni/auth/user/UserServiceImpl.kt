package hr.fer.zavrsni.auth.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import javax.persistence.EntityManager

@Service("userService")
class UserServiceImpl : UserDetailsService, UserService {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var entityManager: EntityManager

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username) ?: throw UsernameNotFoundException("Invalid username or password")

        return org.springframework.security.core.userdetails.User(user.username, user.password, getAuthority(user))
    }

    override fun findAll(): List<User>? {
        return userRepository.findAll()
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun delete(id: Long) {
        userRepository.delete(id)
    }

    override fun findUser(username: String): User? {
        return userRepository.findByUsername(username)
    }

    private fun getAuthority(user: User): List<SimpleGrantedAuthority> {
        return user.roles.map { it.getSimpleAuthority() }
    }

}