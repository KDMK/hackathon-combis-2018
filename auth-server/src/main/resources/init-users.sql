INSERT INTO USERS(email, first_name, last_name, password, username) values ('admin@test', 'admin',
  'adminovic', '$2a$04$l6/HL2q2da5LRBD260dbV.cK0vHT.qQ5MYbAXjHS/4HQOFKNqiXwm', 'admin');

INSERT INTO OAUTH_CLIENT_DETAILS (client_id, client_secret, authorized_grant_types, access_token_validity, web_server_redirect_uri, scope) VALUES ('greengarden-restaurants',
  '$2a$04$HmYirfSggLofSNsKHl05uOcaBt1LDVmEDZBO0pCFX2tuqr4NN.7xC', 'authorization_code', 3600, 'http://localhost:8080/login', 'read,write') -- sample