function invalidateForm() {
  let formInputs = $('.validate-form .input100');

  formInputs.each(function () {
    $(this).focus(function () {
      hideValidate(this);
    });
  });
}

function showValidate(input) {
  const thisAlert = $(input).parent();

  $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
  const thisAlert = $(input).parent();

  $(thisAlert).removeClass('alert-validate');
}


(function ($) {
  "use strict";


  /*==================================================================
  [ Focus input ]*/
  $('.input100').each(function () {
    $(this).on('blur', function () {
      if ($(this).val().trim() != "") {
        $(this).addClass('has-val');
      }
      else {
        $(this).removeClass('has-val');
      }
    })
  });


  /*==================================================================
  [ Validate ]*/
  const inputLogin = $('.validate-input .input100.input-login');
  const inputRegister = $('.validate-input .input100.input-register');

  console.log(inputLogin);
  console.log(inputRegister);

  $('.validate-form.login').on('submit', function () {
    let check = true;

    for (let i = 0; i < inputLogin.length; i++) {
      if (validate(inputLogin[i]) === false) {
        showValidate(inputLogin[i]);
        check = false;
      }
    }

    console.log(check);
    return check;
  });

  $('.validate-form.register').on('submit', function () {
    let check = true;

    for (let i = 0; i < inputRegister.length; i++) {
      if (validate(inputRegister[i]) === false) {
        showValidate(inputRegister[i]);
        check = false;
      }
    }

    if (check) {
      let values = {};
      $(this).serializeArray().forEach(e => values[e.name] = e.value);
      delete values['password-confirm'];

      $.ajax({
        url: "/auth/register",
        method: "POST",
        data: JSON.stringify(values),
        dataType: "json",
        contentType: "application/json; charset=UTF-8",
        statusCode: {
          200: data => {
            window.location.assign("http://localhost:8081/auth/success");
            console.log(data);
            console.log(data.responseText)
          },
          500: jqXHR => {
            window.location.assign("http://localhost:8081/auth/success");
            console.log(jqXHR);
          }
        }
      })
    }

    return false;
  });

  invalidateForm();

  function validate(input) {
    if ($(input).val().trim() === '') {
      return false;
    }

    if ($(input).attr('type') === 'email' || $(input).attr('name') === 'email') {
      return $(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) != null;
    }

    if ($(input).attr('name') === 'password-confirm') {
      const pass = $('#registration-password');
      return $(input).val() === pass.val();
    }

    if ($(input).attr('id') === 'registration-username') {
      const curUsername = $(input).val();
      const usernameExists = $.ajax(`/auth/users/exists/${curUsername}`, {
        method: "GET",
        async: false,
      }).responseText;

      // return usernameExists === 'false';
      return true;
    }
  }

})(jQuery);